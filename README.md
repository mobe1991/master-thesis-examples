IMPORTANT: all commands listed in this README should be run from the repository root.

# How to start the Hadoop cluster

To spin up a Hadoop cluster, a Docker image is used that is either started on your locally installed Docker host if you
are on Linux or inside a docker-machine Linux VM that is automatically created if you are on Windows. In the latter case 
you first need to install Oracle VirtualBox.

To start the cluster, run
```
mvn -Dhadoop.cluster.address=192.168.99.100 -pl . integration-test
```

Substitute `<cluster-ip>` by the expected IP address of the cluster which should be `localhost` on systems with a native
Docker installation or the IP address of the virtual machine created with VirtualBox (192.168.99.100 by default).

NOTE: The first time starting the cluster can take longer because the Docker image needs to be built.