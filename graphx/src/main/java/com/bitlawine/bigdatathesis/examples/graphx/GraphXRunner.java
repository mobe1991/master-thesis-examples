package com.bitlawine.bigdatathesis.examples.graphx;

import com.bitlawine.bigdatathesis.examples.common.graph.TopologyOptimizationExecutor;

import java.io.IOException;

/**
 * @author Moritz Becker on 09/06/2017
 */
public class GraphXRunner {

    public static void main(String[] args) throws IOException {
        if (args.length != 2) {
            System.out.println("Usage: <topology-name> <num-new-stations>");
            System.exit(1);
        }
        String topologyName = args[0];
        int numNewStations = Integer.parseInt(args[1]);

        TopologyOptimizationExecutor.run(new GraphXCompactnessOptimizer(), topologyName, numNewStations);
    }
}
