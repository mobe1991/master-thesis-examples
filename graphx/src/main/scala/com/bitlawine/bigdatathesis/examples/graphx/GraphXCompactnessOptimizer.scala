package com.bitlawine.bigdatathesis.examples.graphx

import java.util

import com.bitlawine.bigdatathesis.examples.common.graph.Vertex.NodeType
import com.bitlawine.bigdatathesis.examples.common.graph.{CompactnessOptimizer, Graph}
import org.apache.spark.graphx.lib.ShortestPaths
import org.apache.spark.{SparkConf, SparkContext}

import scala.collection.JavaConversions

/**
  * @author Moritz Becker on 09/06/2017
  */
class GraphXCompactnessOptimizer extends CompactnessOptimizer {

  def maximizeCompactness(graph: Graph,
                          extensionIter: util.Iterator[Graph]): Graph = {
    val conf = new SparkConf()
      .setAppName("WWAN Station Positioning")
      .setMaster("local[2]")
    val sc = new SparkContext(conf)

    val vertexRdd = sc.parallelize(
      JavaConversions.collectionAsScalaIterable(graph.getVertices)
        .filter(v => util.EnumSet.of(
          NodeType.SENSOR,
          NodeType.STATION
        ).contains(v.getNodeType)
        )
        .map(v => (v.getId, v.getNodeType)).toSeq
    )
    val edges = JavaConversions.asScalaSet(graph.getEdges)
    val edgeRdd = sc.parallelize(
      ( edges.map(e => org.apache.spark.graphx.Edge(
        e.getVertex1.getId,
        e.getVertex2.getId)
      )
        ++
        // add reverse direction edges
        edges.map(e => org.apache.spark.graphx.Edge(
          e.getVertex2.getId,
          e.getVertex1.getId)
        )
        ).toSeq
    )

    var maxCompactness = 0.0
    var bestExtension: Graph = null
    for (extension: Graph <- JavaConversions.asScalaIterator(extensionIter)) {
      val extensionVertexRdd = sc.parallelize(
        JavaConversions.collectionAsScalaIterable(extension.getVertices)
          .map(v => (v.getId, v.getNodeType))
          .toSeq
      )
      // no need to add reverse direction edges because the extension edges
      // always point towards the station vertex and we are not interested
      // in paths starting at station vertices anyway
      val extensionEdgeRdd = sc.parallelize(
        JavaConversions.asScalaSet(extension.getEdges)
          .map(e => org.apache.spark.graphx.Edge(
            e.getVertex1.getId,
            e.getVertex2.getId
          ))
          .toSeq
      )

      val extendedGraph = org.apache.spark.graphx.Graph.apply[NodeType, Nothing](
        vertexRdd ++ extensionVertexRdd,
        edgeRdd ++ extensionEdgeRdd
      )
      val compactness = this.compactness(extendedGraph)
      if (compactness > maxCompactness) {
        maxCompactness = compactness
        bestExtension = extension
      }
    }
    sc.stop()
    bestExtension
  }

  def compactness(graph: org.apache.spark.graphx.Graph[NodeType, Nothing]): Double = {
    val landmarks = graph.vertices
      .filter(v => util.EnumSet.of(
          NodeType.STATION,
          NodeType.ELIGIBLE_NEW_STATION
        ).contains(v._2)
      ).map(v => v._1)
      .collect()

    val shortestPaths = ShortestPaths.run[NodeType, Nothing](
      graph,
      landmarks
    )
    val shortestPathLengths = shortestPaths.vertices
      .innerJoin(graph.vertices)((id, spmap, nodeType) => (spmap, nodeType))
      .flatMap(entry => entry._2._1.map(smapEntry => (entry._1, entry._2._2, smapEntry._1, smapEntry._2)))
      .keyBy(t => t._3)
      .join(graph.vertices)
      // filter out station-to-station paths
      .filter(t =>
        t._2._1._2 == NodeType.SENSOR ||
        t._2._2 == NodeType.SENSOR
      )
      .map(t => (t._2._1._1, t._2._1._4))
      // only consider the length of the path to the closest station
      .reduceByKey((dist1, dist2) => Math.min(dist1, dist2))
      .map(t => t._2)

    val k = shortestPathLengths.max()
    val shortestPathSum = shortestPathLengths.sum()

    val numSensors = graph.vertices
      .filter(v => v._2 == NodeType.SENSOR)
      .count()
    val max = k * numSensors
    val min = numSensors
    val compactness = (max - shortestPathSum) / (max - min)

    println("Shortest Path Landmarks: " + landmarks.length)
    println("Shortest Path Lengths: " + shortestPathLengths.count())
    println("Compactness: (%d - %.2f) / (%d - %d)"
      .format(
        max,
        shortestPathSum,
        max,
        min
      )
    )
    println("Compactness: " + compactness)
    compactness
  }
}
