package com.bitlawine.bigdatathesis.examples.spark;

import com.bitlawine.bigdatathesis.examples.common.batch.AirQualityTuple;
import com.bitlawine.bigdatathesis.examples.common.batch.Indicators;
import com.bitlawine.bigdatathesis.examples.common.batch.Regions;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaSparkContext;
import scala.Tuple2;
import scala.Tuple3;

import java.time.ZonedDateTime;
import java.time.temporal.ChronoField;
import java.time.temporal.TemporalAccessor;
import java.util.Map;

/**
 * @author Moritz Becker on 18/05/2017
 */
public class SparkRunner {

    public static void main(String[] args) {
        SparkConf conf = new SparkConf().setMaster("local[2]").setAppName("AirQuality Report");
        JavaSparkContext sc = new JavaSparkContext(conf);

        Map<Tuple2<Indicators, Regions>, Integer> counts = sc.textFile("data/tuples")
                .mapToPair(SparkRunner::parse)
                .reduceByKey((t1, t2) -> t1.getValue() > t2.getValue() ? t1 : t2)
                .mapToPair(t -> Tuple2.apply(
                        Tuple2.apply(t._1()._1(), t._1()._2()),
                        t._2().getValue() > t._2().getIndicator().getThreshold() ? 1 : 0
                ))
                .reduceByKey((v1, v2) -> v1 + v2)
                .collectAsMap();

        System.out.println(counts);
    }

    private static Tuple2<Tuple3<Indicators, Regions, Integer>, AirQualityTuple> parse(String s) {
        String[] parts = s.split(";");
        Indicators indicator = Indicators.valueOf(parts[0]);
        Regions region = Regions.valueOf(parts[1]);
        Double value = Double.valueOf(parts[2]);
        ZonedDateTime ts = ZonedDateTime.parse(parts[3]);

        return Tuple2.apply(
                Tuple3.apply(
                        indicator,
                        region,
                        ts.query(SparkRunner::queryDayOfYearGroup)
                ), new AirQualityTuple(indicator, region, value, ts)
        );
    }

    private static int queryDayOfYearGroup(TemporalAccessor temporalAccessor) {
        return temporalAccessor.get(ChronoField.MONTH_OF_YEAR) * 31 + temporalAccessor.get(ChronoField.DAY_OF_MONTH);
    }

}
