package com.bitlawine.bigdatathesis.examples.sparkstreaming;

import com.bitlawine.bigdatathesis.examples.common.batch.AirQualityTuple;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.Optional;
import org.apache.spark.storage.StorageLevel;
import org.apache.spark.streaming.Durations;
import org.apache.spark.streaming.State;
import org.apache.spark.streaming.StateSpec;
import org.apache.spark.streaming.api.java.JavaDStream;
import org.apache.spark.streaming.api.java.JavaPairDStream;
import org.apache.spark.streaming.api.java.JavaReceiverInputDStream;
import org.apache.spark.streaming.api.java.JavaStreamingContext;
import scala.Tuple2;

import java.nio.file.Paths;
import java.util.*;
import java.util.logging.Logger;

/**
 * @author Moritz Becker on 07/05/2017
 */
public class SparkStreamingRunner {
    private static final Logger LOG = Logger.getLogger(SparkStreamingRunner.class.getName());

    public static void main(String[] args) throws InterruptedException {
        SparkConf conf = new SparkConf().setMaster("local[2]").setAppName("AirQuality Statistics");
        JavaStreamingContext ssc = new JavaStreamingContext(conf, Durations.seconds(10));
        JavaSparkContext sc = ssc.sparkContext();

        ssc.checkpoint(Paths.get(System.getProperty("user.dir"), ".spark").toAbsolutePath().toString());

        JavaReceiverInputDStream<AirQualityTuple> stream = ssc.receiverStream(new RandomAirQualityReceiver(StorageLevel.MEMORY_AND_DISK_SER_2()));
        stream.checkpoint(Durations.minutes(1));

        // hour min
        JavaDStream<AirQualityTuple> hourMinStream = detectChanges(sc,
                stream
                .mapToPair(t -> Tuple2.apply(new AirQualityTupleGroup(t.getIndicator(), t.getRegion()), t))
                .reduceByKeyAndWindow((t1, t2) -> t1.getValue() < t2.getValue() ? t1 : t2, Durations.minutes(60), Durations.seconds(10))
        );
        print("1hMin", hourMinStream);
        // hour max
        JavaDStream<AirQualityTuple> hourMaxStream = detectChanges(sc,
                stream
                .mapToPair(t -> Tuple2.apply(new AirQualityTupleGroup(t.getIndicator(), t.getRegion()), t))
                .reduceByKeyAndWindow((t1, t2) -> t1.getValue() > t2.getValue() ? t1 : t2, Durations.minutes(60), Durations.seconds(10))
        );
        print("1hMax", hourMaxStream);
        // hour avg
        JavaPairDStream<AirQualityTupleGroup, AirQualityTuple> hourAvgStream = stream
                .mapToPair(t -> Tuple2.apply(new AirQualityTupleGroup(t.getIndicator(), t.getRegion()), t))
                .reduceByKeyAndWindow((t1, t2) -> new AirQualityTuple(t1.getIndicator(), t1.getRegion(), (t1.getValue() + t2.getValue()) / 2, null), Durations.minutes(60), Durations.seconds(10));
        print("1hAvg", hourAvgStream);
        // hour tumbling avg
        JavaPairDStream<AirQualityTupleGroup, AirQualityTuple> hourTumblingAvgStream = stream
                .mapToPair(t -> Tuple2.apply(new AirQualityTupleGroup(t.getIndicator(), t.getRegion()), t))
                .reduceByKeyAndWindow((t1, t2) -> new AirQualityTuple(t1.getIndicator(), t1.getRegion(), (t1.getValue() + t2.getValue()) / 2, null), Durations.minutes(60), Durations.minutes(60));

        // day min
        JavaDStream<AirQualityTuple> dayMinStream = detectChanges(sc, hourMinStream
                .mapToPair(t -> Tuple2.apply(new AirQualityTupleGroup(t.getIndicator(), t.getRegion()), t))
                .reduceByKeyAndWindow((t1, t2) -> t1.getValue() < t2.getValue() ? t1 : t2, Durations.minutes(24 * 60), Durations.seconds(10))
        );
        print("1dMin", dayMinStream);
        // day max
        JavaDStream<AirQualityTuple> dayMaxStream = detectChanges(sc, hourMaxStream
                .mapToPair(t -> Tuple2.apply(new AirQualityTupleGroup(t.getIndicator(), t.getRegion()), t))
                .reduceByKeyAndWindow((t1, t2) -> t1.getValue() > t2.getValue() ? t1 : t2, Durations.minutes(24 * 60), Durations.seconds(10))
        );
        print("1dMax", dayMaxStream);
        // day avg
        JavaPairDStream<AirQualityTupleGroup, AirQualityTuple> dayAvgStream = hourTumblingAvgStream
                .reduceByKeyAndWindow((t1, t2) -> new AirQualityTuple(t1.getIndicator(), t1.getRegion(), (t1.getValue() + t2.getValue()) / 2, null), Durations.minutes(60));
        print("1dAvg", dayAvgStream);

        // week min
        JavaDStream<AirQualityTuple> weekMinStream = detectChanges(sc, dayMinStream
                .mapToPair(t -> Tuple2.apply(new AirQualityTupleGroup(t.getIndicator(), t.getRegion()), t))
                .reduceByKeyAndWindow((t1, t2) -> t1.getValue() < t2.getValue() ? t1 : t2, Durations.minutes(24 * 60), Durations.seconds(10))
        );
        print("1wMin", weekMinStream);
        // week max
        JavaDStream<AirQualityTuple> weekMaxStream = detectChanges(sc, dayMaxStream
                .mapToPair(t -> Tuple2.apply(new AirQualityTupleGroup(t.getIndicator(), t.getRegion()), t))
                .reduceByKeyAndWindow((t1, t2) -> t1.getValue() > t2.getValue() ? t1 : t2, Durations.minutes(24 * 60), Durations.seconds(10))
        );
        print("1wMax", weekMaxStream);
        // week avg
        JavaPairDStream<AirQualityTupleGroup, AirQualityTuple> weekAvgStream = hourTumblingAvgStream
                .reduceByKeyAndWindow((t1, t2) -> new AirQualityTuple(t1.getIndicator(), t1.getRegion(), (t1.getValue() + t2.getValue()) / 2, null), Durations.minutes(60));
        print("1wAvg", weekAvgStream);

        JavaDStream<AirQualityTuple> fiveMinStream = stream
                .mapToPair(t -> Tuple2.apply(new AirQualityTupleGroup(t.getIndicator(), t.getRegion()), t))
                .mapWithState(StateSpec.function((AirQualityTupleGroup group, Optional<AirQualityTuple> optionalTuple, State<AirQualityTuple> state) ->
                    updateAirQualityStepState(optionalTuple.get(), state, 1, 10)
                ).initialState(sc.parallelizePairs(Collections.emptyList())))
                .filter(Optional::isPresent)
                .map(Optional::get);
        print("5min", fiveMinStream);

        JavaDStream<AirQualityTuple> twoHoursStream = stream
                .mapToPair(t -> Tuple2.apply(new AirQualityTupleGroup(t.getIndicator(), t.getRegion()), t))
                .mapWithState(StateSpec.function((AirQualityTupleGroup group, Optional<AirQualityTuple> optionalTuple, State<AirQualityTuple> state) ->
                        updateAirQualityStepState(optionalTuple.get(), state, 1/(60 * 60), 2)
                ).initialState(sc.parallelizePairs(Collections.emptyList())))
                .filter(Optional::isPresent)
                .map(Optional::get);
        print("2hours", twoHoursStream);

        JavaDStream<AirQualityTuple> twelveHoursStream = stream
                .mapToPair(t -> Tuple2.apply(new AirQualityTupleGroup(t.getIndicator(), t.getRegion()), t))
                .mapWithState(StateSpec.function((AirQualityTupleGroup group, Optional<AirQualityTuple> optionalTuple, State<AirQualityTuple> state) ->
                        updateAirQualityStepState(optionalTuple.get(), state, 1/(60 * 60), 12)
                ).initialState(sc.parallelizePairs(Collections.emptyList())))
                .filter(Optional::isPresent)
                .map(Optional::get);
        print("12hours", twelveHoursStream);

        ssc.start();
        ssc.awaitTermination();
    }

    private static Optional<AirQualityTuple> updateAirQualityStepState(AirQualityTuple tuple, State<AirQualityTuple> state, double secondToUnitFactor, int stepSizeInUnits) {
        if (!state.exists()) {
            if (((int) (tuple.getTimestamp().toInstant().getEpochSecond() * secondToUnitFactor)) % stepSizeInUnits == 0) {
                state.update(tuple);
                return Optional.of(tuple);
            } else {
                return Optional.empty();
            }
        } else {
            if (state.get().getTimestamp().isBefore(tuple.getTimestamp())) {
                int stateUnits = (int) (state.get().getTimestamp().toInstant().getEpochSecond() * secondToUnitFactor);
                int tupleUnits = (int) (tuple.getTimestamp().toInstant().getEpochSecond() * secondToUnitFactor);
                if (stateUnits != tupleUnits && tupleUnits % stepSizeInUnits == 0) {
                    state.update(tuple);
                    return Optional.of(tuple);
                }
            }
            return Optional.empty();
        }
    }

    private static <K, V> JavaDStream<V> detectChanges(JavaSparkContext sc, JavaPairDStream<K, V> pairStream) {
        return pairStream.mapWithState(StateSpec
                .function((K tupleGroup, Optional<V> tuple, State<V> state) -> {
                    if (!state.exists() || !state.get().equals(tuple.get())) {
                        state.update(tuple.get());
                        return Optional.of(tuple.get());
                    } else {
                        return Optional.<V> empty();
                    }
                }).initialState(sc.parallelizePairs(Collections.emptyList()))
        ).filter(Optional::isPresent)
        .map(Optional::get);
    }

    private static void print(String streamName, JavaDStream<?> stream) {
        stream.foreachRDD(rdd -> {
            rdd.toLocalIterator().forEachRemaining(t -> LOG.info(streamName + ": " + t.toString()));
        });
    }
    private static void print(String streamName, JavaPairDStream<?, ?> stream) {
        stream.foreachRDD(rdd -> {
            rdd.toLocalIterator().forEachRemaining(t -> LOG.info(streamName + ": " + t._2().toString()));
        });
    }
}
