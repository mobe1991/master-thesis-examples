package com.bitlawine.bigdatathesis.examples.sparkstreaming;

import com.bitlawine.bigdatathesis.examples.common.batch.Indicators;
import com.bitlawine.bigdatathesis.examples.common.batch.Regions;

import java.io.Serializable;

/**
 * @author Moritz Becker on 08/05/2017
 */
public class AirQualityTupleGroup implements Serializable {
    private final Indicators indicator;
    private final Regions region;

    public AirQualityTupleGroup(Indicators indicator, Regions region) {
        this.indicator = indicator;
        this.region = region;
    }

    public Indicators getIndicator() {
        return indicator;
    }

    public Regions getRegion() {
        return region;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof AirQualityTupleGroup)) return false;

        AirQualityTupleGroup that = (AirQualityTupleGroup) o;

        if (indicator != null ? !indicator.equals(that.indicator) : that.indicator != null) return false;
        return region != null ? region.equals(that.region) : that.region == null;
    }

    @Override
    public int hashCode() {
        int result = indicator != null ? indicator.hashCode() : 0;
        result = 31 * result + (region != null ? region.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return String.format("(%s, %s)", indicator, region);
    }
}
