package com.bitlawine.bigdatathesis.examples.sparkstreaming;

import com.bitlawine.bigdatathesis.examples.common.batch.AirQualityTuple;
import com.bitlawine.bigdatathesis.examples.common.batch.Indicators;
import com.bitlawine.bigdatathesis.examples.common.batch.Regions;
import org.apache.spark.storage.StorageLevel;
import org.apache.spark.streaming.receiver.Receiver;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.concurrent.ThreadLocalRandom;

/**
 * @author Moritz Becker on 07/05/2017
 */
public class RandomAirQualityReceiver extends Receiver<AirQualityTuple> {

    private Thread thread;
    private volatile boolean stop;

    public RandomAirQualityReceiver(StorageLevel storageLevel) {
        super(storageLevel);
    }

    @Override
    public void onStart() {
        thread = new Thread(() -> {
            Regions[] regions = Regions.values();
            Indicators[] indicators = Indicators.values();
            while (!stop) {
                Regions region = regions[ThreadLocalRandom.current().nextInt(regions.length)];
                int indicatorIdx = ThreadLocalRandom.current().nextInt(indicators.length);
                Indicators indicator = indicators[indicatorIdx];
                Double indicatorMaxVal = indicator.getMaxValue();
                Double nextVal = ThreadLocalRandom.current().nextDouble(0, indicatorMaxVal + 1);
                AirQualityTuple tuple = new AirQualityTuple(
                        indicator,
                        region,
                        nextVal,
                        ZonedDateTime.now(ZoneId.of("UTC+0"))
                );
                store(tuple);
                try {
                    Thread.sleep(250);
                } catch (InterruptedException e) {
                    Thread.currentThread().interrupt();
                    throw new RuntimeException(e);
                }
            }
        });
        thread.start();
    }

    @Override
    public void onStop() {
        stop = true;
        try {
            thread.join();
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            throw new RuntimeException(e);
        }
    }
}
