package com.bitlawine.bigdatathesis.examples.common.graph;

import org.graphstream.graph.Node;
import org.graphstream.graph.implementations.SingleGraph;
import org.graphstream.ui.swingViewer.GraphRenderer;
import org.graphstream.ui.swingViewer.ViewPanel;
import org.graphstream.ui.view.Viewer;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.Iterator;

/**
 * @author Moritz Becker on 11/06/2017
 */
public class TopologyViewer {

    public static void display(Topology topology) {
        display(topology.getTopologyGraph());
    }

    public static void display(Graph graph) {
        org.graphstream.graph.Graph displayGraph = new SingleGraph("Topology");

        graph.getVertices().forEach(v -> displayGraph.addNode(Long.toString(v.getId())));
        graph.getEdges().forEach(e -> displayGraph.addEdge(
                e.getVertex1().getId() + "-" + e.getVertex2().getId(),
                Long.toString(e.getVertex1().getId()),
                Long.toString(e.getVertex2().getId())
        ));
        displayGraph.getEachNode().forEach(n -> {
            Vertex vertex = graph.getVertex(Long.parseLong(n.getId()));
            n.setAttribute("xy", vertex.getX(), vertex.getY());
            n.setAttribute("ui.class", vertex.getNodeType().name());
            n.setAttribute("ui.label", n.getId());
        });
        displayGraph.addAttribute("ui.stylesheet",
                "node {text-size: 16;} " +
                        "node." + Vertex.NodeType.STATION.name() + "{fill-color: red;} " +
                        "node." + Vertex.NodeType.ELIGIBLE_NEW_STATION.name() + "{fill-color: green;} " +
                        "node." + Vertex.NodeType.NEW_STATION.name() + "{fill-color: violet;}");

        Viewer viewer = new Viewer(displayGraph, Viewer.ThreadingModel.GRAPH_IN_GUI_THREAD);
        GraphRenderer renderer = Viewer.newGraphRenderer();
        ViewPanel panel = viewer.addView(Viewer.DEFAULT_VIEW_ID, renderer);
        panel.addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent e) {
                if (e.getKeyChar() == 'l') {
                    Iterator<Node> iter = displayGraph.getNodeIterator();
                    if (iter.hasNext()) {
                        if (iter.next().hasAttribute("ui.label")) {
                            displayGraph.getNodeSet().forEach(n -> n.removeAttribute("ui.label"));
                        } else {
                            displayGraph.getNodeSet().forEach(n -> n.addAttribute("ui.label", n.getId()));
                        }
                    }
                }
            }

            @Override
            public void keyPressed(KeyEvent e) {
            }

            @Override
            public void keyReleased(KeyEvent e) {
            }
        });
    }

}
