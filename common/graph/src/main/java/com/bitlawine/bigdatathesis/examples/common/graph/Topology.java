package com.bitlawine.bigdatathesis.examples.common.graph;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.Properties;

/**
 * @author Moritz Becker on 11/06/2017
 */
public class Topology {
    private static final String SENSOR_RADIUS_PROPERTY = "sensorRadius";
    private static final String STATION_RADIUS_PROPERTY = "stationRadius";

    private final String name;
    private final int sensorRadius;
    private final int stationRadius;
    private final Graph topologyGraph;

    public Topology(String name, int sensorRadius, int stationRadius, Graph topologyGraph) {
        this.name = name;
        this.sensorRadius = sensorRadius;
        this.stationRadius = stationRadius;
        this.topologyGraph = topologyGraph;
    }

    public String getName() {
        return name;
    }

    public int getSensorRadius() {
        return sensorRadius;
    }

    public int getStationRadius() {
        return stationRadius;
    }

    public Graph getTopologyGraph() {
        return topologyGraph;
    }

    public void saveToDirectory(Path outDir) throws IOException {
        if (Files.notExists(outDir)) {
            Files.createDirectory(outDir);
        }
        try (OutputStream os = Files.newOutputStream(outDir.resolve(name), StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING)) {
            topologyGraph.write(os);
        }
        Properties properties = new Properties();
        properties.setProperty(SENSOR_RADIUS_PROPERTY, Integer.toString(sensorRadius));
        properties.setProperty(STATION_RADIUS_PROPERTY, Integer.toString(stationRadius));
        try (OutputStream os = Files.newOutputStream(outDir.resolve(name + ".properties"), StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING)) {
            properties.store(os, null);
        }
    }

    public static Topology readFromDirectory(Path dir, String topologyName) throws IOException {
        Graph graph;
        try (InputStream is = Files.newInputStream(dir.resolve(topologyName), StandardOpenOption.READ)) {
            graph = Graph.read(is);
        }

        int sensorRadius;
        int stationRadius;
        try (InputStream is = Files.newInputStream(dir.resolve(topologyName + ".properties"), StandardOpenOption.READ)) {
            Properties properties = new Properties();
            properties.load(is);
            sensorRadius = Integer.parseInt(properties.getProperty(SENSOR_RADIUS_PROPERTY));
            stationRadius = Integer.parseInt(properties.getProperty(STATION_RADIUS_PROPERTY));
        }
        return new Topology(topologyName, sensorRadius, stationRadius, graph);
    }

}
