package com.bitlawine.bigdatathesis.examples.common.graph;

import com.bitlawine.bigdatathesis.examples.common.graph.Edge;
import com.bitlawine.bigdatathesis.examples.common.graph.Graph;
import com.bitlawine.bigdatathesis.examples.common.graph.Topology;
import com.bitlawine.bigdatathesis.examples.common.graph.Vertex;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @author Moritz Becker on 16/06/2017
 */
public class SolutionIterator implements Iterator<Graph> {

    private final Topology topology;
    private final List<Vertex> eligibleStationPositions;
    private final int numNewStations;
    private int[] nextPermutationIdx;

    public SolutionIterator(Topology topology, List<Vertex> eligibleStationPositions, int numNewStations) {
        this.topology = topology;
        this.eligibleStationPositions = eligibleStationPositions;
        this.numNewStations = numNewStations;
        permute();
    }

    @Override
    public boolean hasNext() {
        return nextPermutationIdx != null;
    }

    @Override
    public Graph next() {
        if (nextPermutationIdx == null) {
            throw new NoSuchElementException();
        }
        Set<Vertex> permutationVertices = Arrays.stream(nextPermutationIdx).mapToObj(eligibleStationPositions::get).collect(Collectors.toSet());
        Set<Edge> permutationEdges = new HashSet<>();
        topology.getTopologyGraph().getVertices().stream()
                .filter(v -> v.getNodeType() == Vertex.NodeType.SENSOR)
                .forEach(v -> {
                    for (Vertex newStation : permutationVertices) {
                        if (v.distance(newStation) <= topology.getStationRadius()) {
                            // direction is important
                            permutationEdges.add(new Edge(v, newStation));
                        }
                    }
                });

        // prepare next permutation
        permute();
        return new Graph(permutationVertices, permutationEdges);
    }

    private void permute() {
        if (numNewStations >= eligibleStationPositions.size()) {
            if (nextPermutationIdx == null) {
                nextPermutationIdx = new int[eligibleStationPositions.size()];
                for (int i = 0; i < eligibleStationPositions.size(); nextPermutationIdx[i] = i++) ;
            } else {
                nextPermutationIdx = null;
            }
        } else if (nextPermutationIdx == null) {
            nextPermutationIdx = new int[numNewStations];
            for (int i = 0; i < nextPermutationIdx.length; nextPermutationIdx[i] = i++) ;
        } else {
            int i;
            for (i = nextPermutationIdx.length - 1; i >= 0; i--) {
                if (++nextPermutationIdx[i] < eligibleStationPositions.size() - (nextPermutationIdx.length - i - 1)) {
                    break;
                }
            }
            if (i < 0) {
                // no more permutations
                nextPermutationIdx = null;
            } else {
                int base = nextPermutationIdx[i];
                for (i++; i < nextPermutationIdx.length; i++) {
                    nextPermutationIdx[i] = ++base;
                }
            }
        }
    }
}
