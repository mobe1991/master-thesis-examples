package com.bitlawine.bigdatathesis.examples.common.graph;

/**
 * @author Moritz Becker on 11/06/2017
 */
public class Edge {
    private final Vertex vertex1;
    private final Vertex vertex2;

    public Edge(Vertex vertex1, Vertex vertex2) {
        this.vertex1 = vertex1;
        this.vertex2 = vertex2;
    }

    public Vertex getVertex1() {
        return vertex1;
    }

    public Vertex getVertex2() {
        return vertex2;
    }
}
