package com.bitlawine.bigdatathesis.examples.common.graph;

import java.io.IOException;
import java.util.Iterator;

/**
 * @author Moritz Becker on 28/06/2017
 */
public interface CompactnessOptimizer {

    Graph maximizeCompactness(Graph graph, Iterator<Graph> solutionIterator);
}
