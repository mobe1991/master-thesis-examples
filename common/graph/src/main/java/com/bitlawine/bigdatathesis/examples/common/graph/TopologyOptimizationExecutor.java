package com.bitlawine.bigdatathesis.examples.common.graph;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author Moritz Becker on 28/06/2017
 */
public class TopologyOptimizationExecutor {

    public static void run(CompactnessOptimizer optimizer, String topologyName, int numNewStations) {
        Topology topology = null;
        try {
            topology = Topology.readFromDirectory(Paths.get("data"), topologyName);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        List<Vertex> eligibleStationPositions = topology.getTopologyGraph().getVertices().stream()
                .filter(v -> v.getNodeType() == Vertex.NodeType.ELIGIBLE_NEW_STATION)
                .collect(Collectors.toList());

        Graph optimalSolution = optimizer.maximizeCompactness(topology.getTopologyGraph(), new SolutionIterator(topology, eligibleStationPositions, numNewStations));

        // create new topology from solution
        Graph topologyGraph = topology.getTopologyGraph();
        Set<Vertex> vertices = new HashSet<>(topologyGraph.getVertices());
        Collection<Vertex> optimalStations = optimalSolution.getVertices();
        vertices.removeAll(optimalStations);

        Map<Long, Vertex> newStationVertices = new HashMap<>(optimalStations.size());
        optimalStations.forEach(v -> {
            newStationVertices.put(v.getId(), new Vertex(v.getId(), Vertex.NodeType.NEW_STATION, v.getX(), v.getY()));
        });
        vertices.addAll(newStationVertices.values());

        Set<Edge> edges = new HashSet<>(topologyGraph.getEdges());
        edges.addAll(optimalSolution.getEdges().stream()
                .map(e -> new Edge(
                        newStationVertices.getOrDefault(e.getVertex1().getId(), e.getVertex1()),
                        newStationVertices.getOrDefault(e.getVertex2().getId(), e.getVertex2()))
                )
                .collect(Collectors.toList())
        );
        Topology optimizedTopology = new Topology(
                topologyName + "-solved",
                topology.getSensorRadius(),
                topology.getStationRadius(),
                new Graph(vertices, edges)
        );

        try {
            optimizedTopology.saveToDirectory(Paths.get("data"));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        System.out.println("Optimized topology '" + optimizedTopology.getName() + "' successfully stored to disk!");
        TopologyViewer.display(optimizedTopology);
    }
}
