package com.bitlawine.bigdatathesis.examples.common.graph;

import org.graphstream.graph.implementations.SingleGraph;
import org.graphstream.ui.view.Viewer;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.*;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Collectors;

/**
 * @author Moritz Becker on 09/06/2017
 */
public class TopologyTool {

    private static int lastNodeId;

    private enum Mode {
        GENERATE, VIEW
    }

    public static void main(String[] args) throws IOException {
        String topologyName;
        int rasterSize = 400;
        Mode mode;
        if (args.length == 2) {
            if ("-v".equals(args[1])) {
                mode = Mode.VIEW;
                topologyName = args[0];
            } else {
                usage();
                return;
            }
        } else if (args.length == 6){
            if ("-g".equals(args[1])) {
                mode = Mode.GENERATE;
            } else {
                usage();
                return;
            }
        } else {
            usage();
            return;
        }

        topologyName = args[0];
        if (mode == Mode.GENERATE) {
            int numSensors = Integer.parseInt(args[2]);
            int sensorRadius = Integer.parseInt(args[3]);
            int stationRadius = Integer.parseInt(args[4]);
            int numNewStations = Integer.parseInt(args[5]);

            Set<Vertex> sensors = new HashSet<>(numSensors);
            Set<Edge> edges = new HashSet<>();
            for (int i = 0; i < numSensors; i++) {
                sensors.add(new Vertex(
                        lastNodeId++,
                        Vertex.NodeType.SENSOR,
                        ThreadLocalRandom.current().nextFloat() * rasterSize,
                        ThreadLocalRandom.current().nextFloat() * rasterSize));
            }

            for (Vertex n1 : sensors) {
                for (Vertex n2 : sensors) {
                    if (n1.getId() < n2.getId() && n1.distance(n2) <= sensorRadius) {
                        edges.add(new Edge(n1, n2));
                    }
                }
            }

            Graph topologyGraph = new Graph(sensors, edges);
            Set<Graph> components = topologyGraph.getComponents();
            components.forEach(component -> addStationNode(topologyGraph, component, stationRadius));

            // generate eligible positions for new stations
            for (int i = 0; i < numNewStations; i++) {
                topologyGraph.addVertex(new Vertex(
                        lastNodeId++,
                        Vertex.NodeType.ELIGIBLE_NEW_STATION,
                        ThreadLocalRandom.current().nextFloat() * rasterSize,
                        ThreadLocalRandom.current().nextFloat() * rasterSize));
            }

            Topology topology = new Topology(topologyName, sensorRadius, stationRadius, topologyGraph);
            topology.saveToDirectory(Paths.get("data"));

            TopologyViewer.display(topology);

            System.out.println("Topology written successfully! (" + components.size() + " components)");
        } else {
            TopologyViewer.display(Topology.readFromDirectory(Paths.get("data"), topologyName));
        }
    }

    private static void usage() {
        System.out.println("Usage: <topology-name> [-g <num-sensors> <sensor-radius> <station-radius> <num-new-stations>]|[-v]");
        System.exit(1);
    }

    /**
     * We add the station node close to the sensor node that is the farthest away from the component center
     * @param target
     * @param component
     * @param stationRadius
     */
    private static void addStationNode(Graph target, Graph component, int stationRadius) {
        Collection<Vertex> vertices = component.getVertices();
        float componentCenterX = (float) vertices.stream().mapToDouble(Vertex::getX).average().getAsDouble();
        float componentCenterY = (float) vertices.stream().mapToDouble(Vertex::getY).average().getAsDouble();

        Vertex referenceVertex = vertices.stream()
                .max(Comparator.comparingDouble(v -> Math.pow(componentCenterX - v.getX(), 2.0) + Math.pow(componentCenterY - v.getY(), 2.0)))
                .get();

        ThreadLocalRandom rand = ThreadLocalRandom.current();
        float stationX = referenceVertex.getX() + stationRadius * (0.95f - (float) rand.nextDouble(0.45)) * (rand.nextBoolean() ? 1 : -1);
        float maxStationYDifference = (float) Math.sqrt(stationRadius * stationRadius - Math.pow(stationX - referenceVertex.getX(), 2)) * 0.99f;
        float stationY = Math.max(referenceVertex.getY() - maxStationYDifference, Math.min(referenceVertex.getY() + maxStationYDifference, referenceVertex.getY() + stationRadius * (0.95f - (float) rand.nextDouble(0.45)) * (rand.nextBoolean() ? 1 : -1)));
        Vertex stationVertex = new Vertex(lastNodeId++, Vertex.NodeType.STATION, stationX, stationY);

        target.getVertices().stream()
                .filter(v -> v.distance(stationVertex) <= stationRadius && v.getNodeType() == Vertex.NodeType.SENSOR)
                .collect(Collectors.toList())
                .forEach(v -> target.addEdge(new Edge(v, stationVertex)));
    }

}
