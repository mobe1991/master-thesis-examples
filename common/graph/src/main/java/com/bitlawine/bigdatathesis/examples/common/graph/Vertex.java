package com.bitlawine.bigdatathesis.examples.common.graph;

/**
 * @author Moritz Becker on 11/06/2017
 */
public class Vertex {
    private final long id;
    private final NodeType nodeType;
    private final float x;
    private final float y;

    public Vertex(long id, NodeType nodeType, float x, float y) {
        this.id = id;
        this.nodeType = nodeType;
        this.x = x;
        this.y = y;
    }

    public enum NodeType {
        SENSOR, STATION, ELIGIBLE_NEW_STATION, NEW_STATION
    }

    public long getId() {
        return id;
    }

    public NodeType getNodeType() {
        return nodeType;
    }

    public float getX() {
        return x;
    }

    public float getY() {
        return y;
    }

    public double distance(Vertex n) {
        float xDist = this.x - n.x;
        float yDist = this.y - n.y;
        return Math.sqrt(xDist * xDist + yDist * yDist);
    }

    @Override
    public String toString() {
        return String.format("(%d;%s;%.2f;%.2f)", id, nodeType.name(), x, y);
    }
}
