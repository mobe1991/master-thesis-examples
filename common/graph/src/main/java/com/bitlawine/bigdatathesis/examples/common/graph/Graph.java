package com.bitlawine.bigdatathesis.examples.common.graph;

import java.io.*;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author Moritz Becker on 09/06/2017
 */
public class Graph {
    protected final Map<Long, Vertex> vertices;
    protected final Set<Edge> edges;
    protected final Map<Vertex, Set<Edge>> adjacencyMap;

    public Graph(Set<Vertex> vertices, Set<Edge> edges) {
        this.vertices = vertices.stream().collect(Collectors.toMap(Vertex::getId, Function.identity()));
        this.edges = edges;
        this.adjacencyMap = new HashMap<>(vertices.size());
        for (Edge edge : edges) {
            putAdjacentEdge(edge.getVertex1(), edge);
            putAdjacentEdge(edge.getVertex2(), edge);
        }
    }

    private void putAdjacentEdge(Vertex vertex, Edge edge) {
        Set<Edge> adjacentEdges = adjacencyMap.computeIfAbsent(vertex, n -> new HashSet<>());
        adjacentEdges.add(edge);
    }

    public Collection<Vertex> getVertices() {
        return vertices.values();
    }

    public Vertex getVertex(long id) {
        return vertices.get(id);
    }

    public Set<Edge> getEdges() {
        return edges;
    }

    public Set<Graph> getComponents() {
        Set<Graph> components = new HashSet<>();
        Set<Vertex> touched = new HashSet<>();
        for (Vertex vertex : vertices.values()) {
            if (!touched.contains(vertex)) {
                Set<Vertex> componentVertices = new HashSet<>();
                dfs(vertex, componentVertices);
                Set<Edge> componentEdges = componentVertices.stream()
                        .flatMap(v -> adjacencyMap.containsKey(v) ? adjacencyMap.get(v).stream() : Stream.empty())
                        .collect(Collectors.toSet());
                touched.addAll(componentVertices);
                components.add(new Graph(componentVertices, componentEdges));
            }
        }
        return components;
    }

    private void dfs(Vertex root, Set<Vertex> component) {
        Vertex[] stack = new Vertex[vertices.size()];
        Set<Vertex> stackedVertices = new HashSet<>(vertices.size());
        int tos = -1;
        stack[++tos] = root;
        stackedVertices.add(root);

        while (tos >= 0) {
            Vertex current = stack[tos--];
            component.add(current);
            Set<Edge> adjacentEdges = adjacencyMap.get(current);
            if (adjacentEdges != null) {
                for (Edge adjacentEdge : adjacentEdges) {
                    if (adjacentEdge.getVertex1() == current) {
                        if (!stackedVertices.contains(adjacentEdge.getVertex2())) {
                            stack[++tos] = adjacentEdge.getVertex2();
                            stackedVertices.add(adjacentEdge.getVertex2());
                        }
                    } else {
                        if (!stackedVertices.contains(adjacentEdge.getVertex1())) {
                            stack[++tos] = adjacentEdge.getVertex1();
                            stackedVertices.add(adjacentEdge.getVertex1());
                        }
                    }
                }
            }
        }
    }

    public Set<Edge> getAdjacentEdges(Vertex v) {
        return adjacencyMap.get(v);
    }

    public void addEdge(Edge edge) {
        edges.add(edge);
        vertices.put(edge.getVertex1().getId(), edge.getVertex1());
        vertices.put(edge.getVertex2().getId(), edge.getVertex2());
        putAdjacentEdge(edge.getVertex1(), edge);
        putAdjacentEdge(edge.getVertex2(), edge);
    }

    public void addVertex(Vertex vertex) {
        vertices.put(vertex.getId(), vertex);
    }

    public void write(OutputStream outputStream) throws IOException {
        if (!vertices.isEmpty()) {
            BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(outputStream));
            for (Vertex vertex : vertices.values()) {
                bw.write(String.format(Locale.ENGLISH, "%d;%s;%f;%f", vertex.getId(), vertex.getNodeType().name(), vertex.getX(), vertex.getY()));
                bw.newLine();
            }
            bw.newLine();
            for (Edge edge : edges) {
                bw.write(String.format(Locale.ENGLISH, "%d;%d", edge.getVertex1().getId(), edge.getVertex2().getId()));
                bw.newLine();
            }
            bw.flush();
        }
    }

    public static Graph union(Set<Graph> graphs) {
        Set<Vertex> vertices = graphs.stream().flatMap(g -> g.getVertices().stream()).collect(Collectors.toSet());
        Set<Edge> edges = graphs.stream().flatMap(g -> g.getEdges().stream()).collect(Collectors.toSet());
        return new Graph(vertices, edges);
    }

    public static Graph read(InputStream is) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(is));
        String line;

        // read vertices
        Map<Long, Vertex> vertices = new HashMap<>();
        while ((line = br.readLine()) != null) {
            if (line.trim().isEmpty()) {
                break;
            }
            String[] parts = line.split(";");
            long id = Long.parseLong(parts[0]);
            Vertex.NodeType nodeType = Vertex.NodeType.valueOf(parts[1]);
            float x = Float.parseFloat(parts[2]);
            float y = Float.parseFloat(parts[3]);
            vertices.put(id, new Vertex(id, nodeType, x, y));
        }

        // read edges
        Set<Edge> edges = new HashSet<>();
        while ((line = br.readLine()) != null) {
            String[] parts = line.split(";");
            long v1Id = Long.parseLong(parts[0]);
            long v2Id = Long.parseLong(parts[1]);
            Vertex v1 = vertices.get(v1Id);
            Vertex v2 = vertices.get(v2Id);
            if (v1 != null && v2 != null) {
                edges.add(new Edge(v1, v2));
            }
        }

        return new Graph(new HashSet<>(vertices.values()), edges);
    }
}
