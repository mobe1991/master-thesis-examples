package com.bitlawine.bigdatathesis.examples.test;

import com.bitlawine.bigdatathesis.examples.common.graph.Edge;
import com.bitlawine.bigdatathesis.examples.common.graph.Graph;
import com.bitlawine.bigdatathesis.examples.common.graph.Vertex;
import org.junit.Test;

import java.util.Arrays;
import java.util.HashSet;

import static org.junit.Assert.assertEquals;

/**
 * @author Moritz Becker on 11/06/2017
 */
public class GraphTest {

    @Test
    public void getComponentsTest() {
        Vertex[] vertices = new Vertex[] {
                node(0), node(1), node(2), node(3), node(4), node(5), node(6), node(7), node(8), node(9), node(10)
        };
        Edge[] edges = new Edge[] {
                edge(vertices[0], vertices[1]),
                edge(vertices[1], vertices[2]),
                edge(vertices[2], vertices[0]),

                edge(vertices[4], vertices[5]),
                edge(vertices[5], vertices[6]),
                edge(vertices[5], vertices[7]),
                edge(vertices[7], vertices[8]),

                edge(vertices[9], vertices[10])
        };

        Graph g = new Graph(new HashSet<>(Arrays.asList(vertices)), new HashSet<>(Arrays.asList(edges)));

        assertEquals(4, g.getComponents().size());
    }

    private Vertex node(long id) {
        return new Vertex(id, Vertex.NodeType.SENSOR, 0.0f, 0.0f);
    }

    private Edge edge(Vertex n1, Vertex n2) {
        return new Edge(n1, n2);
    }

}
