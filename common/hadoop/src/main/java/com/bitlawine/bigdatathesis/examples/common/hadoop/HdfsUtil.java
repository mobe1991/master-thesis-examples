package com.bitlawine.bigdatathesis.examples.common.hadoop;

import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.LocatedFileStatus;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.fs.RemoteIterator;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.nio.file.StandardOpenOption;
import java.util.*;
import java.util.regex.Pattern;
import java.util.stream.Stream;

/**
 * @author Moritz Becker on 24/06/2017
 */
public class HdfsUtil {

    private static final Pattern REDUCE_RESULT_PART_PATTERN = Pattern.compile("part-r-\\d+");
    private static final Pattern MAP_RESULT_PART_PATTERN = Pattern.compile("part-m-\\d+");

    public static void copyAndMerge(FileSystem fs, Path hdfsSrc, java.nio.file.Path localTarget) throws IOException {
        deleteFileOrDirectory(localTarget);
        fs.copyToLocalFile(hdfsSrc, new Path(localTarget.toString()));
        java.nio.file.Path tmpFilePath = localTarget.getParent().resolve(localTarget.getFileName() + ".tmp");
        try (OutputStream os = Files.newOutputStream(tmpFilePath, StandardOpenOption.CREATE); Stream<java.nio.file.Path> filesToMerge = Files.list(localTarget)) {
            filesToMerge
                    .filter(path -> path.getFileName().toString().matches("part-r-\\d+"))
                    .sorted()
                    .forEach(path -> {
                        try {
                            Files.copy(path, os);
                        } catch (IOException e) {
                            throw new UncheckedIOException(e);
                        }
                    });
        }
        deleteFileOrDirectory(localTarget);
        Files.move(tmpFilePath, localTarget, StandardCopyOption.REPLACE_EXISTING);
    }

    private static void deleteFileOrDirectory(java.nio.file.Path path) throws IOException {
        if (Files.isDirectory(path)) {
            try (Stream<java.nio.file.Path> files = Files.walk(path)) {
                files.sorted(Comparator.reverseOrder())
                        .map(java.nio.file.Path::toFile)
                        .forEach(File::delete);
            }
        } else {
            Files.deleteIfExists(path);
        }
    }

    public static InputStream createMapResultDirectoryInputStream(FileSystem fs, Path hdfsDirectory) throws IOException {
        return createFilteredDirectoryInputStream(fs, hdfsDirectory, MAP_RESULT_PART_PATTERN);
    }

    public static InputStream createReduceResultDirectoryInputStream(FileSystem fs,Path hdfsDirectory) throws IOException {
        return createFilteredDirectoryInputStream(fs, hdfsDirectory, REDUCE_RESULT_PART_PATTERN);
    }

    private static InputStream createFilteredDirectoryInputStream(FileSystem fs, Path hdfsDirectory, Pattern fileNameFilter) throws IOException {
        RemoteIterator<LocatedFileStatus> fileStatusIter = fs.listFiles(hdfsDirectory, false);

        List<Path> inputStreams = new ArrayList<>();
        while (fileStatusIter.hasNext()) {
            LocatedFileStatus fileStatus = fileStatusIter.next();
            if (fileStatus.isFile() && fileNameFilter.matcher(fileStatus.getPath().getName()).matches()) {
                inputStreams.add(fileStatus.getPath());
            }
        }
        return new SequenceInputStream(new HdfsFileEnumeration(fs, inputStreams.iterator()));
    }
}
