package com.bitlawine.bigdatathesis.examples.common.hadoop;

import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;

import java.io.IOException;
import java.io.InputStream;
import java.util.Enumeration;
import java.util.Iterator;

/**
 * @author Moritz Becker on 24/06/2017
 */
public class HdfsFileEnumeration implements Enumeration<InputStream> {

    private final FileSystem fs;
    private final Iterator<Path> filePathIterator;

    public HdfsFileEnumeration(FileSystem fs, Iterator<Path> filePathIterator) {
        this.fs = fs;
        this.filePathIterator = filePathIterator;
    }

    @Override
    public boolean hasMoreElements() {
        return filePathIterator.hasNext();
    }

    @Override
    public InputStream nextElement() {
        try {
            return fs.open(filePathIterator.next());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
