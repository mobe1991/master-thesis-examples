package com.bitlawine.bigdatathesis.examples.common.batch;

/**
 * @author Moritz Becker on 18/05/2017
 */
public enum Regions {
    A("Region A"),
    B("Region B"),
    C("Region C");

    private final String name;

    Regions(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
