package com.bitlawine.bigdatathesis.examples.common.batch;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Locale;
import java.util.Objects;

/**
 * @author Moritz Becker on 07/05/2017
 */
public class AirQualityTuple implements Serializable {
    private final Indicators indicator;
    private final Regions region;
    private final Double value;
    private final ZonedDateTime timestamp;

    public AirQualityTuple(Indicators indicator, Regions region, Double value, ZonedDateTime timestamp) {
        this.indicator = indicator;
        this.region = region;
        this.value = value;
        this.timestamp = timestamp;
    }

    public Indicators getIndicator() {
        return indicator;
    }

    public Regions getRegion() {
        return region;
    }

    public Double getValue() {
        return value;
    }

    public ZonedDateTime getTimestamp() {
        return timestamp;
    }

    @Override
    public String toString() {
        return String.format(Locale.ENGLISH, "(%s, %s, %.2f, %s)", indicator, region, value, Objects.toString(timestamp));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof AirQualityTuple)) return false;

        AirQualityTuple that = (AirQualityTuple) o;

        if (indicator != null ? !indicator.equals(that.indicator) : that.indicator != null) return false;
        if (region != null ? !region.equals(that.region) : that.region != null) return false;
        if (value != null ? !value.equals(that.value) : that.value != null) return false;
        return timestamp != null ? timestamp.equals(that.timestamp) : that.timestamp == null;
    }

    @Override
    public int hashCode() {
        int result = indicator != null ? indicator.hashCode() : 0;
        result = 31 * result + (region != null ? region.hashCode() : 0);
        result = 31 * result + (value != null ? value.hashCode() : 0);
        result = 31 * result + (timestamp != null ? timestamp.hashCode() : 0);
        return result;
    }
}
