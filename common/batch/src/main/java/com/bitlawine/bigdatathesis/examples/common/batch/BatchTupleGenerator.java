package com.bitlawine.bigdatathesis.examples.common.batch;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Locale;
import java.util.concurrent.ThreadLocalRandom;

/**
 * @author Moritz Becker on 18/05/2017
 */
public class BatchTupleGenerator {

    public static void main(String[] args) throws IOException {
        int year;
        if (args.length == 1) {
            year = Integer.parseInt(args[0]);
        } else {
            System.out.println("Usage: <year>");
            System.exit(1);
            return;
        }

        Indicators[] indicators = Indicators.values();
        Regions[] regions = Regions.values();

        Path dataDir = Paths.get("data");
        if (Files.notExists(dataDir)) {
            Files.createDirectory(dataDir);
        }

        try (BufferedWriter bw = Files.newBufferedWriter(dataDir.resolve("tuples"), StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING)) {
            final int hours =  365 * 24;
            for (int i = 0; i < hours; i++) {
                for (int indicatorIdx = 0; indicatorIdx < indicators.length; indicatorIdx++) {
                    for (int regionIdx = 0; regionIdx < regions.length; regionIdx++) {
                        Regions region = regions[regionIdx];
                        Indicators indicator = indicators[indicatorIdx];
                        Double indicatorMaxVal = indicator.getMaxValue();
                        Double nextVal = ThreadLocalRandom.current().nextDouble(0, indicatorMaxVal + 1);
                        LocalDateTime ts = LocalDateTime.of(year, 1, 1, 0, 0);
                        ts = ts.plusHours(i);
                        AirQualityTuple tuple = new AirQualityTuple(
                                indicator,
                                region,
                                nextVal,
                                ts.atZone(ZoneId.of("UTC+0"))
                        );
                        bw.write(format(tuple));
                        bw.newLine();
                    }
                }
            }
        }
        System.out.println("Data successfully generated!");
    }

    private static String format(AirQualityTuple airQualityTuple) {
        return String.format(Locale.ENGLISH, "%s;%s;%f;%s", airQualityTuple.getIndicator(), airQualityTuple.getRegion(), airQualityTuple.getValue(), airQualityTuple.getTimestamp().toString());
    }
}
