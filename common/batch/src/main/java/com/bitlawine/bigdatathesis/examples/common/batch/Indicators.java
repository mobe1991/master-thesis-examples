package com.bitlawine.bigdatathesis.examples.common.batch;

/**
 * @author Moritz Becker on 18/05/2017
 */
public enum Indicators {
    CO2("co2", 500, 450),
    FINE_DUST("fine dust", 200, 170),
    RADIATION("radiation", 50, 30),
    PESTICIDE("pesticide", 100, 40);

    private final String name;
    private final double maxValue;
    private final double threshold;

    Indicators(String name, double maxValue, double threshold) {
        this.name = name;
        this.maxValue = maxValue;
        this.threshold = threshold;
    }

    public String getName() {
        return name;
    }

    public double getMaxValue() {
        return maxValue;
    }

    public double getThreshold() {
        return threshold;
    }
}
