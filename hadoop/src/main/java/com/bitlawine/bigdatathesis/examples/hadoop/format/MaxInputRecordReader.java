package com.bitlawine.bigdatathesis.examples.hadoop.format;

import org.apache.hadoop.io.DoubleWritable;

import java.io.IOException;

/**
 * @author Moritz Becker on 02/06/2017
 */
public class MaxInputRecordReader extends AirQualityDayGroupKeyInputRecordReader<DoubleWritable> {
    @Override
    protected DoubleWritable readValue(String column) throws IOException {
        return new DoubleWritable(Double.parseDouble(column));
    }
}
