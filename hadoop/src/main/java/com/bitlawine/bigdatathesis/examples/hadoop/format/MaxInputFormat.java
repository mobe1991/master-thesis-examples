package com.bitlawine.bigdatathesis.examples.hadoop.format;

import com.bitlawine.bigdatathesis.examples.hadoop.AirQualityDayGroup;
import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.mapreduce.InputSplit;
import org.apache.hadoop.mapreduce.RecordReader;
import org.apache.hadoop.mapreduce.TaskAttemptContext;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;

import java.io.IOException;

/**
 * @author Moritz Becker on 02/06/2017
 */
public class MaxInputFormat extends FileInputFormat<AirQualityDayGroup, DoubleWritable> {

    @Override
    public RecordReader<AirQualityDayGroup, DoubleWritable> createRecordReader(InputSplit split, TaskAttemptContext context) throws IOException, InterruptedException {
        return new MaxInputRecordReader();
    }
}