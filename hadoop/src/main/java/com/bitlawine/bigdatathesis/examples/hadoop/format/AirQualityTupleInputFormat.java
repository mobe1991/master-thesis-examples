package com.bitlawine.bigdatathesis.examples.hadoop.format;

import com.bitlawine.bigdatathesis.examples.common.batch.AirQualityTuple;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.mapreduce.InputSplit;
import org.apache.hadoop.mapreduce.RecordReader;
import org.apache.hadoop.mapreduce.TaskAttemptContext;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;

import java.io.IOException;

/**
 * @author Moritz Becker on 02/06/2017
 */
public class AirQualityTupleInputFormat extends FileInputFormat<NullWritable, AirQualityTuple> {

    @Override
    public RecordReader<NullWritable, AirQualityTuple> createRecordReader(InputSplit split, TaskAttemptContext context) throws IOException, InterruptedException {
        return new AirQualityTupleRecordReader();
    }
}
