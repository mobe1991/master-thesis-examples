package com.bitlawine.bigdatathesis.examples.hadoop.format;

import org.apache.hadoop.io.IntWritable;

import java.io.IOException;

/**
 * @author Moritz Becker on 02/06/2017
 */
public class SumInputRecordReader extends AirQualityDayGroupKeyInputRecordReader<IntWritable> {
    @Override
    protected IntWritable readValue(String column) throws IOException {
        return new IntWritable(Integer.parseInt(column));
    }
}
