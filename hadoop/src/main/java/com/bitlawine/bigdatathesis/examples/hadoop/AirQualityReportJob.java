package com.bitlawine.bigdatathesis.examples.hadoop;

import com.bitlawine.bigdatathesis.examples.common.batch.AirQualityTuple;
import com.bitlawine.bigdatathesis.examples.common.hadoop.HdfsUtil;
import com.bitlawine.bigdatathesis.examples.hadoop.format.AirQualityTupleInputFormat;
import com.bitlawine.bigdatathesis.examples.hadoop.format.MaxInputFormat;
import com.bitlawine.bigdatathesis.examples.hadoop.format.MaxOutputFormat;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.io.UncheckedIOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.nio.file.StandardOpenOption;
import java.time.temporal.ChronoField;
import java.time.temporal.TemporalAccessor;
import java.util.Comparator;
import java.util.OptionalDouble;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

/**
 * @author Moritz Becker on 18/05/2017
 */
public class AirQualityReportJob {

    public static class MaxMapper extends Mapper<NullWritable, AirQualityTuple, AirQualityDayGroup, DoubleWritable> {
        @Override
        protected void map(NullWritable key, AirQualityTuple value, Context context) throws IOException, InterruptedException {
            context.write(
                    new AirQualityDayGroup(
                            value.getIndicator(),
                            value.getRegion(),
                            value.getTimestamp().query(MaxMapper::queryDayOfYearGroup)
                    ), new DoubleWritable(value.getValue())
            );
        }

        private static int queryDayOfYearGroup(TemporalAccessor temporalAccessor) {
            return temporalAccessor.get(ChronoField.MONTH_OF_YEAR) * 31
                    + temporalAccessor.get(ChronoField.DAY_OF_MONTH);
        }
    }

    public static class MaxReducer extends Reducer<AirQualityDayGroup, DoubleWritable, AirQualityDayGroup, DoubleWritable> {
        @Override
        protected void reduce(AirQualityDayGroup key, Iterable<DoubleWritable> values, Context context) throws IOException, InterruptedException {
            OptionalDouble max = StreamSupport.stream(values.spliterator(), false)
                    .mapToDouble(DoubleWritable::get)
                    .max();

            if (max.isPresent()) {
                context.write(key, new DoubleWritable(max.getAsDouble()));
            }
        }
    }

    public static class ThresholdMapper extends Mapper<AirQualityDayGroup, DoubleWritable, AirQualityGroup, IntWritable> {
        @Override
        protected void map(AirQualityDayGroup key, DoubleWritable value, Context context) throws IOException, InterruptedException {
            context.write(
                    new AirQualityGroup(key.getIndicator(), key.getRegion()),
                    new IntWritable(value.get() > key.getIndicator().getThreshold() ? 1 : 0)
            );
        }
    }

    public static class SumReducer extends Reducer<AirQualityGroup, IntWritable, AirQualityGroup, IntWritable> {
        @Override
        protected void reduce(AirQualityGroup key, Iterable<IntWritable> values, Context context) throws IOException, InterruptedException {
            int sum = StreamSupport.stream(values.spliterator(), false)
                    .mapToInt(IntWritable::get)
                    .sum();
            context.write(key, new IntWritable(sum));
        }
    }

    public static void main(String[] args) throws Exception {
        Path hdfsMaxPath = new Path("data/max");
        Path hdfsViolationsSumPath = new Path("data/violationSum");
        java.nio.file.Path localMaxPath = Paths.get("data", "max");
        java.nio.file.Path localViolationsSumPath = Paths.get("data", "violationsSum");

        String hadoopClusterIp = System.getProperty("hadoop.cluster.address", "localhost");
        Configuration conf = new Configuration();
        conf.set("fs.defaultFS", "hdfs://" + hadoopClusterIp + ":9000");
        conf.set("mapreduce.framework.name", "yarn");
        conf.set("yarn.resourcemanager.address", hadoopClusterIp + ":8032");
        conf.set("mapreduce.app-submission.cross-platform", "true");
        conf.set("mapreduce.output.textoutputformat.separator", ";");

        FileSystem fs = FileSystem.get(conf);
        fs.copyFromLocalFile(new Path("data/tuples"), new Path("tuples"));

        if (fs.exists(hdfsMaxPath)) {
            fs.delete(hdfsMaxPath, true);
        }

        Job job1 = Job.getInstance(conf, "Max");
        job1.setJarByClass(AirQualityReportJob.class);

        job1.setMapperClass(MaxMapper.class);
        job1.setCombinerClass(MaxReducer.class);
        job1.setReducerClass(MaxReducer.class);
        job1.setOutputKeyClass(AirQualityDayGroup.class);
        job1.setOutputValueClass(DoubleWritable.class);
        job1.setInputFormatClass(AirQualityTupleInputFormat.class);
        job1.setOutputFormatClass(MaxOutputFormat.class);
        TextInputFormat.addInputPath(job1, new Path("tuples"));
        TextOutputFormat.setOutputPath(job1, hdfsMaxPath);

        job1.waitForCompletion(true);

        HdfsUtil.copyAndMerge(fs, hdfsMaxPath, localMaxPath);

        if (fs.exists(hdfsViolationsSumPath )) {
            fs.delete(hdfsViolationsSumPath, true);
        }

        Job job2 = Job.getInstance(conf, "Violations Sum");
        job2.setJarByClass(AirQualityReportJob.class);
        job2.setMapperClass(ThresholdMapper.class);
        job2.setCombinerClass(SumReducer.class);
        job2.setReducerClass(SumReducer.class);
        job2.setOutputKeyClass(AirQualityGroup.class);
        job2.setOutputValueClass(IntWritable.class);
        job2.setInputFormatClass(MaxInputFormat.class);
        TextInputFormat.addInputPath(job2, hdfsMaxPath);
        TextOutputFormat.setOutputPath(job2, hdfsViolationsSumPath);

        job2.waitForCompletion(true);

        HdfsUtil.copyAndMerge(fs, hdfsViolationsSumPath, localViolationsSumPath);
        fs.close();
    }
}
