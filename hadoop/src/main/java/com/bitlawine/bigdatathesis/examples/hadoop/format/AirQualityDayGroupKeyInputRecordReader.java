package com.bitlawine.bigdatathesis.examples.hadoop.format;

import com.bitlawine.bigdatathesis.examples.common.batch.Indicators;
import com.bitlawine.bigdatathesis.examples.common.batch.Regions;
import com.bitlawine.bigdatathesis.examples.hadoop.AirQualityDayGroup;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.mapreduce.InputSplit;
import org.apache.hadoop.mapreduce.RecordReader;
import org.apache.hadoop.mapreduce.TaskAttemptContext;
import org.apache.hadoop.mapreduce.lib.input.FileSplit;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * @author Moritz Becker on 02/06/2017
 */
public abstract class AirQualityDayGroupKeyInputRecordReader<VAL> extends RecordReader<AirQualityDayGroup, VAL> {
    private BufferedReader fileReader;
    private AirQualityDayGroup currentKey;
    private VAL currentValue;

    @Override
    public void close() throws IOException {
        if (fileReader != null) {
            fileReader.close();
        }
    }

    @Override
    public void initialize(InputSplit split, TaskAttemptContext context) throws IOException, InterruptedException {
        FileSystem fs = FileSystem.get(context.getConfiguration());
        fileReader = new BufferedReader(new InputStreamReader(fs.open(((FileSplit) split).getPath())));
    }

    @Override
    public boolean nextKeyValue() throws IOException, InterruptedException {
        String line = fileReader.readLine();
        if (line == null) {
            return false;
        } else {
            String[] parts = line.split(";");
            Indicators indicator = Indicators.valueOf(parts[0]);
            Regions region = Regions.valueOf(parts[1]);
            int dayGroup = Integer.parseInt(parts[2]);
            currentKey = new AirQualityDayGroup(indicator, region, dayGroup);
            currentValue = readValue(parts[3]);
            return true;
        }
    }

    protected abstract VAL readValue(String column) throws IOException;

    @Override
    public AirQualityDayGroup getCurrentKey() throws IOException, InterruptedException {
        return currentKey;
    }

    @Override
    public VAL getCurrentValue() throws IOException, InterruptedException {
        return currentValue;
    }

    @Override
    public float getProgress() throws IOException {
        return 0;
    }
}
