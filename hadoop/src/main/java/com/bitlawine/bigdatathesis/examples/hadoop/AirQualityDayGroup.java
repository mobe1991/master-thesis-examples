package com.bitlawine.bigdatathesis.examples.hadoop;

import com.bitlawine.bigdatathesis.examples.common.batch.Indicators;
import com.bitlawine.bigdatathesis.examples.common.batch.Regions;
import org.apache.hadoop.io.WritableComparable;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.util.Locale;

/**
 * @author Moritz Becker on 18/05/2017
 */
public class AirQualityDayGroup implements WritableComparable<AirQualityDayGroup> {
    private Indicators indicator;
    private Regions region;
    private int dayGroup;

    public AirQualityDayGroup() {}

    public AirQualityDayGroup(Indicators indicator, Regions region, int dayGroup) {
        this.indicator = indicator;
        this.region = region;
        this.dayGroup = dayGroup;
    }

    public Indicators getIndicator() {
        return indicator;
    }

    public Regions getRegion() {
        return region;
    }

    public int getDayGroup() {
        return dayGroup;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof AirQualityDayGroup)) return false;

        AirQualityDayGroup that = (AirQualityDayGroup) o;

        if (dayGroup != that.dayGroup) return false;
        if (indicator != that.indicator) return false;
        return region == that.region;
    }

    @Override
    public int hashCode() {
        int result = indicator != null ? indicator.hashCode() : 0;
        result = 31 * result + (region != null ? region.hashCode() : 0);
        result = 31 * result + dayGroup;
        return result;
    }

    @Override
    public int compareTo(AirQualityDayGroup other) {
        int indicatorCmp;
        int regionCmp;
        return (indicatorCmp = indicator.compareTo(other.indicator)) == 0 ?
                ((regionCmp = region.compareTo(other.region)) == 0 ? Integer.compare(dayGroup, other.dayGroup) : regionCmp) : indicatorCmp;
    }

    @Override
    public void write(DataOutput out) throws IOException {
        out.writeInt(indicator.ordinal());
        out.writeInt(region.ordinal());
        out.writeInt(dayGroup);
    }

    @Override
    public void readFields(DataInput in) throws IOException {
        indicator = Indicators.values()[in.readInt()];
        region = Regions.values()[in.readInt()];
        dayGroup = in.readInt();
    }

    @Override
    public String toString() {
        return String.format(Locale.ENGLISH, "(%s, %s)", indicator, region);
    }
}
