package com.bitlawine.bigdatathesis.examples.hadoop.format;

import com.bitlawine.bigdatathesis.examples.hadoop.AirQualityDayGroup;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.mapreduce.InputSplit;
import org.apache.hadoop.mapreduce.RecordReader;
import org.apache.hadoop.mapreduce.TaskAttemptContext;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;

import java.io.IOException;

/**
 * @author Moritz Becker on 02/06/2017
 */
public class SumInputFormat extends FileInputFormat<AirQualityDayGroup, IntWritable> {

    @Override
    public RecordReader<AirQualityDayGroup, IntWritable> createRecordReader(InputSplit split, TaskAttemptContext context) throws IOException, InterruptedException {
        return new SumInputRecordReader();
    }
}