package com.bitlawine.bigdatathesis.examples.hadoop.format;

import com.bitlawine.bigdatathesis.examples.common.batch.AirQualityTuple;
import com.bitlawine.bigdatathesis.examples.common.batch.Indicators;
import com.bitlawine.bigdatathesis.examples.common.batch.Regions;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.mapreduce.InputSplit;
import org.apache.hadoop.mapreduce.RecordReader;
import org.apache.hadoop.mapreduce.TaskAttemptContext;
import org.apache.hadoop.mapreduce.lib.input.FileSplit;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.time.ZonedDateTime;

/**
 * @author Moritz Becker on 02/06/2017
 */
public class AirQualityTupleRecordReader extends RecordReader<NullWritable, AirQualityTuple> {

    private BufferedReader fileReader;
    private AirQualityTuple currentAirQualityTuple;

    @Override
    public void close() throws IOException {
        if (fileReader != null) {
            fileReader.close();
        }
    }

    @Override
    public void initialize(InputSplit split, TaskAttemptContext context) throws IOException, InterruptedException {
        FileSystem fs = FileSystem.get(context.getConfiguration());
        fileReader = new BufferedReader(new InputStreamReader(fs.open(((FileSplit) split).getPath())));
    }

    @Override
    public boolean nextKeyValue() throws IOException, InterruptedException {
        String line = fileReader.readLine();
        if (line == null) {
            return false;
        } else {
            String[] parts = line.split(";");

            currentAirQualityTuple = new AirQualityTuple(
                    Indicators.valueOf(parts[0]),
                    Regions.valueOf(parts[1]),
                    Double.valueOf(parts[2]),
                    ZonedDateTime.parse(parts[3]));
            return true;
        }
    }

    @Override
    public NullWritable getCurrentKey() throws IOException, InterruptedException {
        return currentAirQualityTuple == null ? null : NullWritable.get();
    }

    @Override
    public AirQualityTuple getCurrentValue() throws IOException, InterruptedException {
        return currentAirQualityTuple;
    }

    @Override
    public float getProgress() throws IOException {
        return 0;
    }
}
