package com.bitlawine.bigdatathesis.examples.hadoop.format;

import com.bitlawine.bigdatathesis.examples.hadoop.AirQualityDayGroup;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.mapreduce.RecordWriter;
import org.apache.hadoop.mapreduce.TaskAttemptContext;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;

/**
 * @author Moritz Becker on 02/06/2017
 */
public class ViolationsOutputFormat extends FileOutputFormat<AirQualityDayGroup, IntWritable> {
    @Override
    public RecordWriter<AirQualityDayGroup, IntWritable> getRecordWriter(TaskAttemptContext context) throws IOException, InterruptedException {
        FileSystem fs = FileSystem.get(context.getConfiguration());
        final PrintWriter pw = new PrintWriter(new OutputStreamWriter(fs.create(getDefaultWorkFile(context, ""), false)));
        return new RecordWriter<AirQualityDayGroup, IntWritable>() {
            @Override
            public void write(AirQualityDayGroup key, IntWritable value) throws IOException, InterruptedException {
                pw.println(String.format("%s;%s;%d;%d",
                        key.getIndicator().name(),
                        key.getRegion().name(),
                        key.getDayGroup(),
                        value.get()));
            }

            @Override
            public void close(TaskAttemptContext context) throws IOException, InterruptedException {
                pw.close();
            }
        };
    }
}
