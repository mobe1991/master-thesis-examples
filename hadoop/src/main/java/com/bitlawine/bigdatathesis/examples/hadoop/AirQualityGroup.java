package com.bitlawine.bigdatathesis.examples.hadoop;

import com.bitlawine.bigdatathesis.examples.common.batch.Indicators;
import com.bitlawine.bigdatathesis.examples.common.batch.Regions;
import org.apache.hadoop.io.WritableComparable;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

/**
 * @author Moritz Becker on 02/06/2017
 */
public class AirQualityGroup implements WritableComparable<AirQualityGroup> {
    private Indicators indicator;
    private Regions region;

    public AirQualityGroup() {}

    public AirQualityGroup(Indicators indicator, Regions region) {
        this.indicator = indicator;
        this.region = region;
    }

    public Indicators getIndicator() {
        return indicator;
    }

    public Regions getRegion() {
        return region;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof AirQualityGroup)) return false;

        AirQualityGroup that = (AirQualityGroup) o;

        if (indicator != that.indicator) return false;
        return region == that.region;
    }

    @Override
    public int hashCode() {
        int result = indicator != null ? indicator.hashCode() : 0;
        result = 31 * result + (region != null ? region.hashCode() : 0);
        return result;
    }

    @Override
    public int compareTo(AirQualityGroup other) {
        int indicatorCmp;
        return (indicatorCmp = indicator.compareTo(other.indicator)) == 0 ? region.compareTo(other.region) : indicatorCmp;
    }

    @Override
    public void write(DataOutput out) throws IOException {
        out.writeInt(indicator.ordinal());
        out.writeInt(region.ordinal());
    }

    @Override
    public void readFields(DataInput in) throws IOException {
        indicator = Indicators.values()[in.readInt()];
        region = Regions.values()[in.readInt()];
    }

    @Override
    public String toString() {
        return String.format("(%s, %s)", indicator, region);
    }
}
