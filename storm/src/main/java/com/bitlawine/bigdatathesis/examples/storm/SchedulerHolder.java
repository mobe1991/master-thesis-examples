package com.bitlawine.bigdatathesis.examples.storm;

import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.impl.StdSchedulerFactory;

/**
 * @author Moritz Becker on 05/05/2017
 */
public class SchedulerHolder {

    private static Scheduler scheduler;

    public static void initialize() {
        if (scheduler == null) {
            try {
                // Grab the Scheduler instance from the Factory
                scheduler = StdSchedulerFactory.getDefaultScheduler();

                // and start it off
                scheduler.start();
            } catch (SchedulerException se) {
                throw new RuntimeException(se);
            }
        }
    }

    public static Scheduler getScheduler() {
        return scheduler;
    }

}
