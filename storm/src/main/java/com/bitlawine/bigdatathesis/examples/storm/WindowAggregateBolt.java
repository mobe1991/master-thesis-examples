package com.bitlawine.bigdatathesis.examples.storm;

import java.util.List;

import org.apache.storm.topology.IWindowedBolt;
import org.apache.storm.tuple.Tuple;

/**
 * @author Moritz Becker on 05/05/2017
 */
public interface WindowAggregateBolt extends IWindowedBolt {

    List<Object> aggregate(List<Tuple> tuples);

}
