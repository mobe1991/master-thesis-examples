package com.bitlawine.bigdatathesis.examples.storm;

import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.tuple.Fields;
import org.apache.storm.tuple.Tuple;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Moritz Becker on 03/05/2017
 */
public class AvgBolt extends AbstractWindowAggregateBolt {
    private final Fields fields;
    private final String numberField;

    public AvgBolt(Fields fields, String numberField) {
        super(fields);
        this.fields = fields;
        this.numberField = numberField;
    }

    @Override
    public List<Object> aggregate(List<Tuple> tuples) {
        if (tuples.isEmpty()) {
            return null;
        }
        Tuple firstTuple = tuples.get(0);
        Double avg = tuples.stream().mapToDouble(t -> t.getDoubleByField(numberField)).average().orElse(0);
        List<Object> tuple = new ArrayList<>(fields.size());
        for (String field : fields) {
            if (field.equals(numberField)) {
                tuple.add(avg);
            } else {
                tuple.add(firstTuple.getValueByField(field));
            }
        }
        return tuple;
    }

    @Override
    public void declareOutputFields(OutputFieldsDeclarer declarer) {
        declarer.declare(fields);
    }
}
