package com.bitlawine.bigdatathesis.examples.storm;

import org.apache.storm.tuple.Fields;
import org.apache.storm.tuple.Tuple;

import java.util.Comparator;
import java.util.List;

/**
 * @author Moritz Becker on 03/05/2017
 */
public class MaxBolt extends AbstractWindowAggregateBolt {
    private final String numberField;

    public MaxBolt(Fields fields, String numberField) {
        super(fields);
        this.numberField = numberField;
    }

    @Override
    public List<Object> aggregate(List<Tuple> tuples) {
        Tuple max = tuples.stream().max(Comparator.comparingDouble(t -> t.getDoubleByField(numberField))).orElse(null);
        return max == null ? null : max.getValues();
    }
}
