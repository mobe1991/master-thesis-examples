package com.bitlawine.bigdatathesis.examples.storm;

import org.apache.storm.Config;
import org.apache.storm.LocalCluster;
import org.apache.storm.generated.StormTopology;
import org.apache.storm.topology.TopologyBuilder;
import org.apache.storm.topology.base.BaseWindowedBolt;
import org.apache.storm.trident.spout.RichSpoutBatchExecutor;
import org.apache.storm.tuple.Fields;
import org.quartz.SchedulerException;

import java.io.IOException;
import java.io.Serializable;
import java.util.*;
import java.util.function.Supplier;

/**
 * @author Moritz Becker on 03/05/2017
 */
public class StormRunner {

    public static void main(String[] args) {
        SchedulerHolder.initialize();
        LocalCluster localCluster = new LocalCluster();

        Map<String, Object> topologyConfig = new HashMap<>();
        topologyConfig.put(Config.TOPOLOGY_DEBUG, false);
        topologyConfig.put(Config.TOPOLOGY_MESSAGE_TIMEOUT_SECS, 8 * 24 * 60 * 60);
        topologyConfig.put(Config.TOPOLOGY_DISRUPTOR_WAIT_TIMEOUT_MILLIS, 0);
        topologyConfig.put(Config.TOPOLOGY_DISRUPTOR_BATCH_SIZE, 1);
        topologyConfig.put(Config.TOPOLOGY_DISRUPTOR_BATCH_TIMEOUT_MILLIS, 200);
        topologyConfig.put(RichSpoutBatchExecutor.MAX_BATCH_SIZE_CONF, 10);
        localCluster.submitTopology("airQualityTopology", topologyConfig, buildTopology());

        try {
            System.in.read();
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            SchedulerHolder.getScheduler().shutdown();
        } catch (SchedulerException e) {
            throw new RuntimeException();
        } finally {
            localCluster.shutdown();
        }
    }

    private static StormTopology buildTopology() {
        TopologyBuilder topologyBuilder = new TopologyBuilder();

        final String spoutId = RandomAirQualitySpout.class.getSimpleName();
        final String hourMinBoltId = "1hMinBolt";
        final String hourMaxBoltId = "1hMaxBolt";
        final String hourAvgBoltId = "1hAvgBolt";
        final String dayMinBoltId = "1dMinBolt";
        final String dayMaxBoltId = "1dMaxBolt";
        final String hourTumblingAvgBoltId = "1hTumblingAvgBolt";
        final String dayAvgBoltId = "1dAvgBolt";
        final String weekMinBoltId = "1wMinBolt";
        final String weekMaxBoltId = "1wMaxBolt";
        final String weekAvgBoltId = "1wAvgBolt";
        final String printerBoltId = PrinterBolt.class.getName();

        final String quartzSpout5min = "5minQuartzSpout";
        final String quartzSpout2h = "2hQuartzSpout";
        final String quartzSpout12h = "12hQuartzSpout";
        final String diagramStepBoltId = DiagramStepBolt.class.getSimpleName();

        Map<String, Integer> airQualityIndicators = new HashMap<>();
        airQualityIndicators.put("co2", 500);
        airQualityIndicators.put("fine dust", 200);
        airQualityIndicators.put("radiation", 50);
        airQualityIndicators.put("pesticide", 100);
        Set<String> regions = new HashSet<>(Arrays.asList("Region A", "Region B", "Region C"));

        Fields groupFields = new Fields("field", "region");

        topologyBuilder.setSpout(spoutId, new RandomAirQualitySpout(airQualityIndicators, regions));

        // hour min
        topologyBuilder.setBolt(hourMinBoltId, new GroupedWindowAggregatorBolt(
                    groupFields,
                    (Serializable & Supplier<WindowAggregateBolt>) () -> new ChangeDetectingWindowAggregateBoltWrapper(new MinBolt(RandomAirQualitySpout.AIR_QUALITY_TUPLE_FIELDS, "value"), RandomAirQualitySpout.AIR_QUALITY_TUPLE_FIELDS.fieldIndex("value"))
                ).withWindow(BaseWindowedBolt.Duration.hours(1), BaseWindowedBolt.Duration.seconds(10))
                .withTimestampField("timestamp"))
                .fieldsGrouping(spoutId, new Fields("field", "region"));
        // hour max
        topologyBuilder.setBolt(hourMaxBoltId, new GroupedWindowAggregatorBolt(
                    groupFields,
                    (Serializable & Supplier<WindowAggregateBolt>) () -> new ChangeDetectingWindowAggregateBoltWrapper(new MaxBolt(RandomAirQualitySpout.AIR_QUALITY_TUPLE_FIELDS, "value"), RandomAirQualitySpout.AIR_QUALITY_TUPLE_FIELDS.fieldIndex("value"))
                ).withWindow(BaseWindowedBolt.Duration.hours(1), BaseWindowedBolt.Duration.seconds(10))
                .withTimestampField("timestamp"))
                .fieldsGrouping(spoutId, new Fields("field", "region"));
        // hour avg
        topologyBuilder.setBolt(hourAvgBoltId, new GroupedWindowAggregatorBolt(
                    groupFields,
                    (Serializable & Supplier<WindowAggregateBolt>) () -> new AvgBolt(RandomAirQualitySpout.AIR_QUALITY_TUPLE_FIELDS, "value")
                ).withWindow(BaseWindowedBolt.Duration.hours(1), BaseWindowedBolt.Duration.seconds(10))
                .withTimestampField("timestamp"))
                .fieldsGrouping(spoutId, new Fields("field", "region"));
        // hour tumbling avg
        topologyBuilder.setBolt(hourTumblingAvgBoltId, new GroupedWindowAggregatorBolt(
                    groupFields,
                    (Serializable & Supplier<WindowAggregateBolt>) () -> new AvgBolt(RandomAirQualitySpout.AIR_QUALITY_TUPLE_FIELDS, "value")
                ).withTumblingWindow(BaseWindowedBolt.Duration.hours(1))
                .withTimestampField("timestamp"))
                .fieldsGrouping(spoutId, new Fields("field", "region"));

        // day min
        topologyBuilder.setBolt(dayMinBoltId, new GroupedWindowAggregatorBolt(
                    groupFields,
                    (Serializable & Supplier<WindowAggregateBolt>) () -> new ChangeDetectingWindowAggregateBoltWrapper(new MinBolt(RandomAirQualitySpout.AIR_QUALITY_TUPLE_FIELDS, "value"), RandomAirQualitySpout.AIR_QUALITY_TUPLE_FIELDS.fieldIndex("value"))
                ).withWindow(BaseWindowedBolt.Duration.days(1))
                .withTimestampField("timestamp"))
                .fieldsGrouping(hourMinBoltId, new Fields("field", "region"));
        // day max
        topologyBuilder.setBolt(dayMaxBoltId, new GroupedWindowAggregatorBolt(
                    groupFields,
                    (Serializable & Supplier<WindowAggregateBolt>) () -> new ChangeDetectingWindowAggregateBoltWrapper(new MaxBolt(RandomAirQualitySpout.AIR_QUALITY_TUPLE_FIELDS, "value"), RandomAirQualitySpout.AIR_QUALITY_TUPLE_FIELDS.fieldIndex("value"))
                ).withWindow(BaseWindowedBolt.Duration.days(1))
                .withTimestampField("timestamp"))
                .fieldsGrouping(hourMaxBoltId, new Fields("field", "region"));
        // day avg
        topologyBuilder.setBolt(dayAvgBoltId, new GroupedWindowAggregatorBolt(
                    groupFields,
                    (Serializable & Supplier<WindowAggregateBolt>) () -> new AvgBolt(RandomAirQualitySpout.AIR_QUALITY_TUPLE_FIELDS, "value")
                ).withWindow(BaseWindowedBolt.Duration.days(1))
                .withTimestampField("timestamp"))
                .fieldsGrouping(hourTumblingAvgBoltId, new Fields("field", "region"));

        // week min
        topologyBuilder.setBolt(weekMinBoltId, new GroupedWindowAggregatorBolt(
                    groupFields,
                    (Serializable & Supplier<WindowAggregateBolt>) () -> new ChangeDetectingWindowAggregateBoltWrapper(new MinBolt(RandomAirQualitySpout.AIR_QUALITY_TUPLE_FIELDS, "value"), RandomAirQualitySpout.AIR_QUALITY_TUPLE_FIELDS.fieldIndex("value"))
                ).withWindow(BaseWindowedBolt.Duration.days(7))
                .withTimestampField("timestamp"))
                .fieldsGrouping(dayMinBoltId, new Fields("field", "region"));
        // week max
        topologyBuilder.setBolt(weekMaxBoltId, new GroupedWindowAggregatorBolt(
                    groupFields,
                    (Serializable & Supplier<WindowAggregateBolt>) () -> new ChangeDetectingWindowAggregateBoltWrapper(new MaxBolt(RandomAirQualitySpout.AIR_QUALITY_TUPLE_FIELDS, "value"), RandomAirQualitySpout.AIR_QUALITY_TUPLE_FIELDS.fieldIndex("value"))
                ).withWindow(BaseWindowedBolt.Duration.days(7))
                .withTimestampField("timestamp"))
                .fieldsGrouping(dayMaxBoltId, new Fields("field", "region"));
        // week avg
        topologyBuilder.setBolt(weekAvgBoltId, new GroupedWindowAggregatorBolt(
                    groupFields,
                    (Serializable & Supplier<WindowAggregateBolt>) () -> new AvgBolt(RandomAirQualitySpout.AIR_QUALITY_TUPLE_FIELDS, "value")
                ).withWindow(BaseWindowedBolt.Duration.days(7))
                .withTimestampField("timestamp"))
                .fieldsGrouping(hourTumblingAvgBoltId, new Fields("field", "region"));

        // diagram step handling
        /*
         * for testing, you might want to change this to
         * - "0/10 * * * * ?"
         * - "0 0/2 * * * ?"
         * - "0 0/12 * * * ?"
         */
        topologyBuilder.setSpout(quartzSpout5min, new QuartzSpout("5min", "0 0/5 * * * ?"));
        topologyBuilder.setSpout(quartzSpout2h, new QuartzSpout("2hours", "0 0 0/2 * * ?"));
        topologyBuilder.setSpout(quartzSpout12h, new QuartzSpout("12hours", "0 0 0/12 * * ?"));

        // diagram step bolts
        topologyBuilder.setBolt(diagramStepBoltId, new DiagramStepBolt())
                .fieldsGrouping(spoutId, new Fields("field", "region"))
                .allGrouping(quartzSpout5min)
                .allGrouping(quartzSpout2h)
                .allGrouping(quartzSpout12h);

        topologyBuilder.setBolt(printerBoltId, new PrinterBolt())
                .shuffleGrouping(hourMinBoltId)
                .shuffleGrouping(hourMaxBoltId)
                .shuffleGrouping(hourAvgBoltId)
                .shuffleGrouping(dayMinBoltId)
                .shuffleGrouping(dayMaxBoltId)
                .shuffleGrouping(dayAvgBoltId)
                .shuffleGrouping(weekMinBoltId)
                .shuffleGrouping(weekMaxBoltId)
                .shuffleGrouping(weekAvgBoltId);

        return topologyBuilder.createTopology();
    }
}
