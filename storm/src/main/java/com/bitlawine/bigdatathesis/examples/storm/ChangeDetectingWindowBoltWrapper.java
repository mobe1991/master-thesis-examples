package com.bitlawine.bigdatathesis.examples.storm;

import java.util.*;

import org.apache.storm.task.OutputCollector;
import org.apache.storm.task.TopologyContext;
import org.apache.storm.topology.IWindowedBolt;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.tuple.Tuple;
import org.apache.storm.windowing.TimestampExtractor;
import org.apache.storm.windowing.TupleWindow;

/**
 * @author Moritz Becker on 05/05/2017
 */
public class ChangeDetectingWindowBoltWrapper implements IWindowedBolt {

    private final int[] changeableFieldIndices;
    private final IWindowedBolt wrappedBolt;

    public ChangeDetectingWindowBoltWrapper(IWindowedBolt wrappedBolt, int... changeableFieldIndices) {
        this.changeableFieldIndices = changeableFieldIndices;
        this.wrappedBolt = wrappedBolt;
    }

    @Override
    public void prepare(Map stormConf, TopologyContext context, OutputCollector collector) {
        wrappedBolt.prepare(stormConf, context, new OutputCollector(collector) {

            private final Map<String, List<Object>> latestChanges = new HashMap<>();

            @Override
            public List<Integer> emit(String streamId, Collection<Tuple> anchors, List<Object> tuple) {
                List<Object> latest = latestChanges.put(streamId, tuple);
                if (latest == null) {
                    return super.emit(streamId, anchors, tuple);
                } else {
                    boolean changed = false;
                    for (int changeableFieldIdx : changeableFieldIndices) {
                        if (!tuple.get(changeableFieldIdx).equals(latest.get(changeableFieldIdx))) {
                            changed = true;
                            break;
                        }
                    }
                    if (changed) {
                        return super.emit(streamId, anchors, tuple);
                    } else {
                        return Collections.emptyList();
                    }
                }
            }
        });
    }

    @Override
    public void execute(TupleWindow inputWindow) {
        wrappedBolt.execute(inputWindow);
    }

    @Override
    public void cleanup() {
        wrappedBolt.cleanup();
    }

    @Override
    public TimestampExtractor getTimestampExtractor() {
        return wrappedBolt.getTimestampExtractor();
    }

    @Override
    public void declareOutputFields(OutputFieldsDeclarer declarer) {
        wrappedBolt.declareOutputFields(declarer);
    }

    @Override
    public Map<String, Object> getComponentConfiguration() {
        return wrappedBolt.getComponentConfiguration();
    }
}
