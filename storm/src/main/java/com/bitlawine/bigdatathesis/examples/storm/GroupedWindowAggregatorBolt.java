package com.bitlawine.bigdatathesis.examples.storm;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Supplier;

import org.apache.storm.task.OutputCollector;
import org.apache.storm.task.TopologyContext;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.topology.base.BaseWindowedBolt;
import org.apache.storm.tuple.Fields;
import org.apache.storm.tuple.Tuple;
import org.apache.storm.windowing.TupleWindow;
import org.apache.storm.windowing.TupleWindowImpl;

/**
 * @author Moritz Becker on 04/05/2017
 */
public class GroupedWindowAggregatorBolt extends BaseWindowedBolt {

    private final Fields groupFields;
    private final Supplier<WindowAggregateBolt> aggregateBoltSupplier;

    private Map<List<Object>, TupleWindow> groupedWindows;
    private Map<List<Object>, WindowAggregateBolt> aggregateBolts;
    private Map stormConf;
    private TopologyContext context;
    private OutputCollector outputCollector;

    public GroupedWindowAggregatorBolt(Fields groupFields, Supplier<WindowAggregateBolt> aggregateBoltSupplier) {
        this.groupFields = groupFields;
        this.aggregateBoltSupplier = aggregateBoltSupplier;
    }

    @Override
    public void prepare(Map stormConf, TopologyContext context, OutputCollector collector) {
        this.stormConf = stormConf;
        this.context = context;
        this.outputCollector = collector;

        groupedWindows = new HashMap<>();
        aggregateBolts = new HashMap<>();
    }

    @Override
    public void execute(TupleWindow inputWindow) {
        for (Tuple tuple : inputWindow.getNew()) {
            List<Object> groupValues = tuple.select(groupFields);
            TupleWindow tupleWindow = groupedWindows.get(groupValues);
            if (tupleWindow == null) {
                List<Tuple> newTuples = new ArrayList<>();
                newTuples.add(tuple);
                tupleWindow = new TupleWindowImpl(new ArrayList<>(newTuples), newTuples, new ArrayList<>());
                groupedWindows.put(groupValues, tupleWindow);
            } else {
                tupleWindow.getNew().add(tuple);
                tupleWindow.get().add(tuple);
            }
        }
        for (Tuple tuple : inputWindow.getExpired()) {
            List<Object> groupValues = tuple.select(groupFields);
            TupleWindow tupleWindow = groupedWindows.get(groupValues);
            if (tupleWindow == null) {
                throw new RuntimeException("Grouped TupleWindow not present for group values: " + groupValues);
            } else {
                tupleWindow.getExpired().add(tuple);
                tupleWindow.get().remove(tuple);
            }
        }
        for (Map.Entry<List<Object>, TupleWindow> groupedWindowEntry : groupedWindows.entrySet()) {
            getOrCreateBolt(groupedWindowEntry.getKey()).execute(groupedWindowEntry.getValue());
        }
    }

    private WindowAggregateBolt getOrCreateBolt(List<Object> groupValues) {
        return aggregateBolts.computeIfAbsent(groupValues, values -> {
            WindowAggregateBolt aggregateBolt = aggregateBoltSupplier.get();
            aggregateBolt.prepare(stormConf, context, outputCollector);
            return aggregateBolt;
        });
    }

    @Override
    public void cleanup() {
        groupedWindows.clear();
        aggregateBolts.values().forEach(WindowAggregateBolt::cleanup);
        aggregateBolts.clear();
    }

    @Override
    public void declareOutputFields(OutputFieldsDeclarer declarer) {
        aggregateBoltSupplier.get().declareOutputFields(declarer);
    }
}
