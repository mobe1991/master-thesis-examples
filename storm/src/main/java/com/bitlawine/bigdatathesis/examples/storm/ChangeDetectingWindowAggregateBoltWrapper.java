package com.bitlawine.bigdatathesis.examples.storm;

import java.util.List;

import org.apache.storm.topology.IWindowedBolt;
import org.apache.storm.tuple.Tuple;

/**
 * @author Moritz Becker on 05/05/2017
 */
public class ChangeDetectingWindowAggregateBoltWrapper extends ChangeDetectingWindowBoltWrapper implements WindowAggregateBolt {

    public ChangeDetectingWindowAggregateBoltWrapper(IWindowedBolt wrappedBolt, int... changeableFieldIndices) {
        super(wrappedBolt, changeableFieldIndices);
    }

    @Override
    public List<Object> aggregate(List<Tuple> tuples) {
        throw new UnsupportedOperationException();
    }
}
