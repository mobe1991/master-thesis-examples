package com.bitlawine.bigdatathesis.examples.storm;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ThreadLocalRandom;
import java.util.logging.Logger;

import org.apache.storm.Config;
import org.apache.storm.spout.SpoutOutputCollector;
import org.apache.storm.task.TopologyContext;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.topology.base.BaseRichSpout;
import org.apache.storm.tuple.Fields;
import org.apache.storm.tuple.Values;

/**
 * @author Moritz Becker on 03/05/2017
 */
public class RandomAirQualitySpout extends BaseRichSpout {

    private static final Logger LOG = Logger.getLogger(RandomAirQualitySpout.class.getName());
    public static final Fields AIR_QUALITY_TUPLE_FIELDS = new Fields(
            "field",
            "region",
            "value",
            "timestamp"
    );

    private final Map<String, Integer> airQualityIndicators;
    private final List<String> airQualityIndicatorNames;
    private final List<String> regions;
    private SpoutOutputCollector collector;

    public RandomAirQualitySpout(Map<String, Integer> airQualityIndicators, Set<String> regions) {
        this.airQualityIndicators = airQualityIndicators;
        this.airQualityIndicatorNames = new ArrayList<>(airQualityIndicators.keySet());
        this.regions = new ArrayList<>(regions);
    }

    @Override
    public void open(Map conf, TopologyContext context, SpoutOutputCollector collector) {
        this.collector = collector;
    }

    @Override
    public void close() {
    }

    @Override
    public void nextTuple() {
        String region = regions.get(ThreadLocalRandom.current().nextInt(regions.size()));
        int indicatorIdx = ThreadLocalRandom.current().nextInt(airQualityIndicatorNames.size());
        String indicatorName = airQualityIndicatorNames.get(indicatorIdx);
        Integer indicatorMaxVal = airQualityIndicators.get(indicatorName);
        Double nextVal = ThreadLocalRandom.current().nextDouble(0, indicatorMaxVal + 1);
        Values tuple = new Values(
                indicatorName,
                region,
                nextVal,
                Instant.now().toEpochMilli()
        );
        collector.emit(tuple);
        try {
            Thread.sleep(250);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            throw new RuntimeException(e);
        }
    }

    @Override
    public void declareOutputFields(OutputFieldsDeclarer declarer) {
        declarer.declare(AIR_QUALITY_TUPLE_FIELDS);
    }

    @Override
    public Map<String, Object> getComponentConfiguration() {
        Config conf = new Config();
        conf.setMaxTaskParallelism(1);
        return conf;
    }

    @Override
    public void ack(Object msgId) {
        LOG.info("Message processing successful for " + msgId);
    }

    @Override
    public void fail(Object msgId) {
        LOG.warning("Message processing failed for " + msgId);
    }
}
