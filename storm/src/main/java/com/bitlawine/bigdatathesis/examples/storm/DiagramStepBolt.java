package com.bitlawine.bigdatathesis.examples.storm;

import java.time.Instant;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import org.apache.storm.topology.BasicOutputCollector;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.topology.base.BaseBasicBolt;
import org.apache.storm.tuple.Fields;
import org.apache.storm.tuple.Tuple;

/**
 * @author Moritz Becker on 04/05/2017
 */
public class DiagramStepBolt extends BaseBasicBolt {

    private static final Logger LOG = Logger.getLogger(DiagramStepBolt.class.getName());
    private final Map<List<Object>, Tuple> currentValues = new HashMap<>();

    @Override
    public void declareOutputFields(OutputFieldsDeclarer declarer) {
    }

    @Override
    public void execute(Tuple input, BasicOutputCollector collector) {
        if (isTriggerTuple(input)) {
            String triggerName = input.getStringByField("trigger");
            Instant triggerTime = Instant.ofEpochMilli(input.getLongByField("timestamp"));
            StringBuilder sb = new StringBuilder("\n");
            for (Tuple storedTuple : currentValues.values()) {
                sb.append('\t');
                sb.append(storedTuple);
                sb.append('\n');
            }
            LOG.info("Store tuples for trigger " + triggerName + "/" + triggerTime + ": " + sb);
        } else {
            List<Object> groupValues = input.select(new Fields("field", "region"));
            currentValues.put(groupValues, input);
        }
    }

    private boolean isTriggerTuple(Tuple tuple) {
        return tuple.contains("trigger");
    }

    @Override
    public void cleanup() {
        currentValues.clear();
    }
}
