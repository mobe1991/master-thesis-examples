package com.bitlawine.bigdatathesis.examples.storm;

import static org.quartz.JobBuilder.*;
import static org.quartz.TriggerBuilder.*;

import java.time.Instant;
import java.util.Map;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

import org.apache.storm.spout.SpoutOutputCollector;
import org.apache.storm.task.TopologyContext;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.topology.base.BaseRichSpout;
import org.apache.storm.tuple.Fields;
import org.apache.storm.tuple.Values;
import org.quartz.*;

/**
 * @author Moritz Becker on 04/05/2017
 */
public class QuartzSpout extends BaseRichSpout {

    private final String triggerName;
    private final JobKey jobKey;
    private final String cronExpression;
    private final Queue<Instant> triggers = new ConcurrentLinkedQueue<>();
    private SpoutOutputCollector collector;

    public QuartzSpout(String triggerName, String cronExpression) {
        this.triggerName = triggerName;
        this.jobKey = JobKey.jobKey(triggerName + "Job");
        this.cronExpression = cronExpression;
    }

    @Override
    public void open(Map conf, TopologyContext context, SpoutOutputCollector collector) {
        this.collector = collector;
        try {
            // define the job and tie it to our HelloJob class
            JobDataMap jobDataMap = new JobDataMap();
            jobDataMap.put("queue", triggers);
            JobDetail job = newJob(SchedulerJob.class)
                    .usingJobData(jobDataMap)
                    .withIdentity(jobKey)
                    .build();

            // Trigger the job to run now, and then repeat every 40 seconds
            Trigger trigger = newTrigger()
                    .withIdentity(triggerName)
                    .startNow()
                    .withSchedule(CronScheduleBuilder.cronSchedule(cronExpression))
                    .build();

            // Tell quartz to schedule the job using our trigger
            SchedulerHolder.getScheduler().scheduleJob(job, trigger);
        } catch (SchedulerException se) {
            throw new RuntimeException(se);
        }
    }

    @Override
    public void close() {
        try {
            SchedulerHolder.getScheduler().deleteJob(jobKey);
        } catch (SchedulerException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void nextTuple() {
        Instant trigger = triggers.poll();
        if (trigger == null) {
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
                throw new RuntimeException(e);
            }
        } else {
            collector.emit(new Values(triggerName, trigger.getEpochSecond() * 1000));
        }
    }

    @Override
    public void declareOutputFields(OutputFieldsDeclarer declarer) {
        declarer.declare(new Fields("trigger", "timestamp"));
    }

    public static class SchedulerJob implements Job {

        @Override
        public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
            Queue<Instant> queue = (Queue<Instant>) jobExecutionContext.getJobDetail().getJobDataMap().get("queue");
            queue.offer(jobExecutionContext.getFireTime().toInstant());
        }

    }
}
