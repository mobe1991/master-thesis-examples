package com.bitlawine.bigdatathesis.examples.storm;

import org.apache.storm.task.OutputCollector;
import org.apache.storm.task.TopologyContext;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.topology.base.BaseWindowedBolt;
import org.apache.storm.tuple.Fields;
import org.apache.storm.windowing.TupleWindow;

import java.util.List;
import java.util.Map;

/**
 * @author Moritz Becker on 03/05/2017
 */
public abstract class AbstractWindowAggregateBolt extends BaseWindowedBolt implements WindowAggregateBolt {
    private final Fields fields;
    private OutputCollector collector;
    
    public AbstractWindowAggregateBolt(Fields fields) {
        this.fields = fields;
    }

    @Override
    public void prepare(Map stormConf, TopologyContext context, OutputCollector collector) {
        this.collector = collector;
    }

    @Override
    public void execute(TupleWindow inputWindow) {
        List<Object> aggregate = aggregate(inputWindow.get());
        if (aggregate != null) {
            collector.emit(aggregate);
        }
    }

    @Override
    public void declareOutputFields(OutputFieldsDeclarer declarer) {
        declarer.declare(fields);
    }
}
