Use maven to run this example. 

IMPORTANT: all commands listed in this README should be run from the repository root.

# Start the Hadoop cluster

See the README in the repository root for instructions on how to start the cluster.

# Generate data

Follow the README in common/graph to generate some test data.

# Run the example

To start the Giraph topology optimizer, run

```
-Dhadoop.cluster.address=<cluster-ip> -Dtopology=<topology-name> -DnumNewStations=<num-new-stations> -pl giraph -am integration-test
```

Substitute
* `<cluster-ip>` by the IP address of the host running the Hadoop cluster (see [above](#start-the-hadoop-cluster))
* `<topology-name>` by the name of the topology you generated in [step 2](#generate-data)
* `<num-new-stations>` by the number of new stations that should be selected for placement from the eligible stations (aka budget)

NOTE: The optimizer performs brute-force optimizations, i.e. it will go through all subsets of size `<num-new-stations>`
from the set of eligible stations. The number of subsets becomes large very quickly and henceforth, the execution
might take very long. Thus you should choose the parameter `<num-new-stations>` close to the total number of eligible
stations in order to reduce the complexity.