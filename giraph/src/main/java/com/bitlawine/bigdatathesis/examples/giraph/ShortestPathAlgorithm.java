package com.bitlawine.bigdatathesis.examples.giraph;

import com.bitlawine.bigdatathesis.examples.giraph.format.ShortestPathMap;
import org.apache.giraph.Algorithm;
import org.apache.giraph.bsp.CentralizedServiceWorker;
import org.apache.giraph.comm.WorkerClientRequestProcessor;
import org.apache.giraph.conf.StrConfOption;
import org.apache.giraph.graph.BasicComputation;
import org.apache.giraph.graph.GraphState;
import org.apache.giraph.graph.Vertex;
import org.apache.giraph.worker.WorkerGlobalCommUsage;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.NullWritable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * @author Moritz Becker on 16/06/2017
 */
@Algorithm(name = "shortest path")
public class ShortestPathAlgorithm extends BasicComputation<LongWritable, ShortestPathMap, NullWritable, ShortestPathMap> {
    private static final Logger LOG = LoggerFactory.getLogger(ShortestPathAlgorithm.class);
    public static final StrConfOption TARGET_IDS =
            new StrConfOption("shortestPath.targetIds", "",
                    "The target vertex ids to compute the shortest paths to");

    private Set<Long> targetVertexIds;

    @Override
    public void initialize(GraphState graphState, WorkerClientRequestProcessor<LongWritable, ShortestPathMap, NullWritable> workerClientRequestProcessor, CentralizedServiceWorker<LongWritable, ShortestPathMap, NullWritable> serviceWorker, WorkerGlobalCommUsage workerGlobalCommUsage) {
        super.initialize(graphState, workerClientRequestProcessor, serviceWorker, workerGlobalCommUsage);

        targetVertexIds = Arrays.stream(TARGET_IDS.get(getConf()).split("-"))
                .filter(s -> !s.isEmpty())
                .map(Long::valueOf)
                .collect(Collectors.toSet());
    }

    @Override
    public void compute(
            Vertex<LongWritable, ShortestPathMap, NullWritable> vertex,
            Iterable<ShortestPathMap> messages) throws IOException {
        if (getSuperstep() == 0) {
            initState(vertex);
            LOG.debug("Initial state vertex " + vertex.getId() + ": " + vertex.getValue());
        }

        // shortest path computation
        ShortestPathMap updatedState = vertex.getValue();
        for (ShortestPathMap msg : messages) {
            updatedState = mergeMaps(updatedState, msg);
        }

        if ((getSuperstep() == 0 && !updatedState.isEmpty()) ||
                !updatedState.equals(vertex.getValue())) {
            vertex.setValue(updatedState);
            ShortestPathMap incrementedState = increment(updatedState);
            sendMessageToAllEdges(vertex, incrementedState);
        }

        vertex.voteToHalt();
    }

    private ShortestPathMap mergeMaps(ShortestPathMap map1, ShortestPathMap map2) {
        Set<Long> combinedKeySet = new HashSet<>(map1.keySet());
        combinedKeySet.addAll(map2.keySet());
        return combinedKeySet.stream()
                .collect(Collectors.toMap(
                        Function.identity(),
                        vertexId -> Math.min(map1.getOrDefault(vertexId, Integer.MAX_VALUE), map2.getOrDefault(vertexId, Integer.MAX_VALUE)),
                        (a, b) -> a,
                        ShortestPathMap::new
                ));
    }

    private ShortestPathMap increment(ShortestPathMap shortestPathMap) {
        return shortestPathMap.entrySet()
                .stream()
                .collect(Collectors.toMap(
                        Map.Entry::getKey,
                        entry -> entry.getValue() + 1,
                        (a, b) -> a,
                        ShortestPathMap::new
                ));
    }

    private void initState(Vertex<LongWritable, ShortestPathMap, NullWritable> vertex) {
        ShortestPathMap newState = new ShortestPathMap();
        if (isTarget(vertex)) {
            newState.put(vertex.getId().get(), 0);
        }
        vertex.setValue(newState);
    }

    private boolean isTarget(Vertex<LongWritable, ?, ?> vertex) {
        return targetVertexIds.contains(vertex.getId().get());
    }
}
