package com.bitlawine.bigdatathesis.examples.giraph;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;

/**
 * @author Moritz Becker on 28/06/2017
 */
public class GlobalReductionMapper extends Mapper<LongWritable, Text, NullWritable, Text> {

    private final Text valueWrapper = new Text();

    @Override
    protected void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
        int reduceColumn = context.getConfiguration().getInt("reduceColumn", -1);
        valueWrapper.set(value.toString().split(";")[reduceColumn]);
        context.write(NullWritable.get(), valueWrapper);
    }
}
