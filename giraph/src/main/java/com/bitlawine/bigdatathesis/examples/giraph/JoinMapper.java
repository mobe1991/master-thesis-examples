package com.bitlawine.bigdatathesis.examples.giraph;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.lib.input.FileSplit;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * @author Moritz Becker on 24/06/2017
 */
public class JoinMapper extends Mapper<LongWritable, Text, Text, Text> {

    private static final Pattern PART_NAME_PATTERN = Pattern.compile("part-[m|r]-\\d{5}");

    private int keyIndex;
    private Text keyWrapper = new Text();
    private Text data = new Text();
    private int joinOrder;
    private String separator;

    @Override
    protected void setup(Mapper.Context context) throws IOException, InterruptedException {
        separator = context.getConfiguration().get("separator");
        FileSplit fileSplit = (FileSplit) context.getInputSplit();

        String logicalFileName = getDirectoryOrFileNameFromSplit(fileSplit);
        joinOrder = context.getConfiguration().getInt(logicalFileName + "-order", -1);
        keyIndex = context.getConfiguration().getInt(logicalFileName + "-keyIndex", -1);
    }

    @Override
    protected void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
        List<String> values = new ArrayList<>(Arrays.asList(value.toString().split(separator)));
        String joinKey = values.remove(keyIndex);
        values.add(0, Integer.toString(joinOrder));
        String valuesWithOutKey = values.stream().collect(Collectors.joining(separator));
        keyWrapper.set(joinKey);
        data.set(valuesWithOutKey);
        context.write(keyWrapper, data);
    }

    private static String getDirectoryOrFileNameFromSplit(FileSplit fileSplit) {
        if (PART_NAME_PATTERN.matcher(fileSplit.getPath().getName()).matches()) {
            return fileSplit.getPath().getParent().getName();
        } else {
            return fileSplit.getPath().getName();
        }
    }

}