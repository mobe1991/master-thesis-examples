package com.bitlawine.bigdatathesis.examples.giraph;

import com.bitlawine.bigdatathesis.examples.common.graph.Vertex;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author Moritz Becker on 26/06/2017
 */
public class FilteringMinPathMapper extends Mapper<LongWritable, Text, LongWritable, Text> {

    private LongWritable keyWrapper = new LongWritable();

    @Override
    protected void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
        List<String> columns = new ArrayList<>(Arrays.asList(
                value.toString().split(";")
        ));
        Vertex.NodeType nodeType1 = Vertex.NodeType.valueOf(columns.get(1));
        Vertex.NodeType nodeType2 = Vertex.NodeType.valueOf(columns.get(4));

        if (nodeType2 == Vertex.NodeType.SENSOR) {
            keyWrapper.set(Long.parseLong(columns.get(2)));
            context.write(keyWrapper, value);
        }
    }
}
