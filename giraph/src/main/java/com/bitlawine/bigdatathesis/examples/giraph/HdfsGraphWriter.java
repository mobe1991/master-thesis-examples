package com.bitlawine.bigdatathesis.examples.giraph;

import com.bitlawine.bigdatathesis.examples.common.graph.Graph;
import com.bitlawine.bigdatathesis.examples.common.graph.Vertex;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Moritz Becker on 16/06/2017
 */
public class HdfsGraphWriter extends BufferedWriter {

    public HdfsGraphWriter(Writer out) {
        super(out);
    }

    public HdfsGraphWriter(Writer out, int sz) {
        super(out, sz);
    }

    public void write(Graph graph) throws IOException {
        if (!graph.getVertices().isEmpty()) {
            Iterator<Vertex> iter = graph.getVertices().iterator();
            writeVertex(graph, iter.next());
            while (iter.hasNext()) {
                newLine();
                writeVertex(graph, iter.next());
            }
        }
    }

    private void writeVertex(Graph graph, Vertex v) throws IOException {
        List<Long> outVertexIds = graph.getAdjacentEdges(v).stream()
                // Giraph assumes directed graphs, so to get an undirected graph we output each edge twice, with inverted
                // direction
                .map(e -> e.getVertex1().getId() == v.getId() ? e.getVertex2().getId() : e.getVertex1().getId())
                .collect(Collectors.toList());
        write(toString(v.getId(), outVertexIds));
    }

    private static String toString(Long vertexId, Collection<Long> outVertexIds) {
        String outVertexIdStr = outVertexIds == null ? null : outVertexIds.stream()
                .map(Object::toString)
                .collect(Collectors.joining(",", "[", "]"));
        return String.format("%d;%s",
                vertexId,
                outVertexIdStr);
    }
}
