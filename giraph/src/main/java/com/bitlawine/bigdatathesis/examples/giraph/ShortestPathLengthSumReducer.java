package com.bitlawine.bigdatathesis.examples.giraph;

import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;
import java.util.stream.StreamSupport;

/**
 * @author Moritz Becker on 26/06/2017
 */
public class ShortestPathLengthSumReducer extends Reducer<NullWritable, Text, NullWritable, Text> {

    private Text valueWrapper = new Text();

    @Override
    protected void reduce(NullWritable key, Iterable<Text> values, Context context) throws IOException, InterruptedException {
        int sum = StreamSupport.stream(values.spliterator(), false)
                .mapToInt(text -> Integer.parseInt(text.toString()))
                .sum();

        valueWrapper.set(Integer.toString(sum));
        context.write(NullWritable.get(), valueWrapper);
    }
}
