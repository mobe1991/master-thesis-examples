package com.bitlawine.bigdatathesis.examples.giraph;

import com.bitlawine.bigdatathesis.examples.common.graph.CompactnessOptimizer;
import com.bitlawine.bigdatathesis.examples.common.graph.Edge;
import com.bitlawine.bigdatathesis.examples.common.graph.Graph;
import com.bitlawine.bigdatathesis.examples.common.graph.Vertex;
import com.bitlawine.bigdatathesis.examples.common.hadoop.HdfsUtil;
import com.bitlawine.bigdatathesis.examples.giraph.format.ShortestPathVertexOutputFormat;
import com.bitlawine.bigdatathesis.examples.giraph.format.VertexInputFormat;
import org.apache.giraph.conf.GiraphConfiguration;
import org.apache.giraph.io.formats.GiraphFileInputFormat;
import org.apache.giraph.yarn.GiraphYarnClient;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.LocalFileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.hdfs.DistributedFileSystem;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.yarn.exceptions.YarnException;

import java.io.*;
import java.nio.file.Files;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author Moritz Becker on 16/06/2017
 */
public class GiraphCompactnessOptimizer implements CompactnessOptimizer {

    private final String jarLocation;
    private final Configuration hadoopConf;
    private FileSystem fs;
    private final String hadoopClusterIp;

    public GiraphCompactnessOptimizer(String jarLocation) {
        this.jarLocation = jarLocation;
        this.hadoopClusterIp = System.getProperty("hadoop.cluster.address", "localhost");
        this.hadoopConf = new Configuration();
        hadoopConf.set("fs.defaultFS", "hdfs://" + hadoopClusterIp + ":9000");
        hadoopConf.set("mapreduce.framework.name", "yarn");
        hadoopConf.set("yarn.resourcemanager.address", hadoopClusterIp + ":8032");
        hadoopConf.set("mapreduce.jobtracker.address", "yarn");
        hadoopConf.set("mapreduce.app-submission.cross-platform", "true");
        hadoopConf.set("mapreduce.output.textoutputformat.separator", ";");

        hadoopConf.set("fs.hdfs.impl",
                DistributedFileSystem.class.getName()
        );
        hadoopConf.set("fs.file.impl",
                LocalFileSystem.class.getName()
        );
    }

    @Override
    public Graph maximizeCompactness(Graph graph, Iterator<Graph> solutionIterator) {
        try {
            this.fs = FileSystem.get(hadoopConf);

            Set<Vertex> baseVertices = new HashSet<>();
            for (Vertex v : graph.getVertices()) {
                if (EnumSet.of(Vertex.NodeType.SENSOR, Vertex.NodeType.STATION).contains(v.getNodeType())) {
                    baseVertices.add(v);
                }
            }
            Set<Edge> baseEdges = graph.getEdges();

            Graph bestExtension = null;
            double maxCompactness = 0.0;
            while (solutionIterator.hasNext()) {
                Graph extension = solutionIterator.next();

                double compactness = compactness(extendBaseGraph(baseVertices, baseEdges, extension));
                if (compactness > maxCompactness) {
                    maxCompactness = compactness;
                    bestExtension = extension;
                }
            }

            this.fs.close();
            return bestExtension;
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private double compactness(Graph graph) throws IOException {
        Path shortestPaths = new Path("data/graph-paths");
        if (fs.exists(shortestPaths)) {
            fs.delete(shortestPaths, true);
        }

        Path hdfsGraphPath = new Path("data/graph");
        Path vertexDetail = new Path("data/vertices");
        try (HdfsGraphWriter graphWriter = new HdfsGraphWriter(new OutputStreamWriter(fs.create(hdfsGraphPath)))) {
            graphWriter.write(graph);
        }
        try (BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(fs.create(vertexDetail)))) {
            for (Vertex v : graph.getVertices()) {
                bw.write(String.format("%d;%s", v.getId(), v.getNodeType().name()));
                bw.newLine();
            }
        }

        String landmarksOption = graph.getVertices().stream()
                .filter(v -> EnumSet.of(Vertex.NodeType.STATION, Vertex.NodeType.ELIGIBLE_NEW_STATION).contains(v.getNodeType()))
                .map(v -> Long.toString(v.getId()))
                .collect(Collectors.joining("-"));

        GiraphConfiguration giraphConfiguration = new GiraphConfiguration(hadoopConf);
        giraphConfiguration.setVertexInputFormatClass(
                VertexInputFormat.class);
        giraphConfiguration.setVertexOutputFormatClass(
                ShortestPathVertexOutputFormat.class);
        giraphConfiguration.setStrings(ShortestPathAlgorithm.TARGET_IDS.getKey(),
                landmarksOption);
        giraphConfiguration.setWorkerConfiguration(1, 2, 100.0f);
        giraphConfiguration.setYarnLibJars(jarLocation);
        giraphConfiguration.setComputationClass(ShortestPathAlgorithm.class);
        GiraphFileInputFormat.addVertexInputPath(giraphConfiguration, hdfsGraphPath);
        giraphConfiguration.set(FileOutputFormat.OUTDIR, shortestPaths.toString());

        GiraphYarnClient giraphYarnClient = new GiraphYarnClient(giraphConfiguration, getClass().getName());
        try {
            if (!giraphYarnClient.run(true)) {
                throw new RuntimeException("Giraph job failed.");
            }
        } catch (YarnException e) {
            throw new RuntimeException(e);
        }

        Path joinOutput = new Path("data/joinOut");
        Path shortestPathToStation = new Path("data/shortestPathToStation");
        joinVertexDetails(shortestPaths, vertexDetail, joinOutput);
        reduceShortestPathToStation(joinOutput, shortestPathToStation);

        int k = getMaxShortestPathLength(shortestPathToStation);
        int shortestPathSum = getShortestPathLengthSum(shortestPathToStation);

        long numSensors = graph.getVertices().stream().filter(v -> v.getNodeType() == Vertex.NodeType.SENSOR).count();
        long max = k * numSensors;
        long min = numSensors;
        return (double) (max - shortestPathSum) / (max - min);
    }

    private void joinVertexDetails(Path shortestPathDir, Path vertexDetailFile, Path joinOut) throws IOException {
        Path intermediaryJoinOutputPath = new Path("data/joinOut_");
        if (fs.exists(intermediaryJoinOutputPath)) {
            fs.delete(intermediaryJoinOutputPath, true);
        }
        if (fs.exists(joinOut)) {
            fs.delete(joinOut, true);
        }

        join(Arrays.asList(shortestPathDir, vertexDetailFile), Arrays.asList(0, 0), intermediaryJoinOutputPath, ";");
        join(Arrays.asList(vertexDetailFile, intermediaryJoinOutputPath), Arrays.asList(0, 1), joinOut, ";");

        if (fs.exists(intermediaryJoinOutputPath)) {
            fs.delete(intermediaryJoinOutputPath, true);
        }
    }

    private void reduceShortestPathToStation(Path joinedShortestPathDir, Path shortestPathToStationOut) {
        try {
            if (fs.exists(shortestPathToStationOut)) {
                fs.delete(shortestPathToStationOut, true);
            }
            Configuration config = new Configuration(hadoopConf);

            Job job = Job.getInstance(config, "Shortest Path to Station");

            job.setJarByClass(GiraphCompactnessOptimizer.class);
            job.setMapperClass(FilteringMinPathMapper.class);
            job.setReducerClass(FilteringMinPathReducer.class);
            job.setMapOutputKeyClass(LongWritable.class);
            job.setOutputKeyClass(NullWritable.class);
            job.setOutputValueClass(Text.class);
            job.setInputFormatClass(TextInputFormat.class);
            job.setOutputFormatClass(TextOutputFormat.class);
            TextInputFormat.addInputPath(job, joinedShortestPathDir);
            TextOutputFormat.setOutputPath(job, shortestPathToStationOut);

            job.waitForCompletion(true);
        } catch (IOException | ClassNotFoundException e) {
            throw new RuntimeException(e);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            throw new RuntimeException(e);
        }
    }

    private int getMaxShortestPathLength(Path shortestPathDir) {
        try {
            Path maxOut = new Path("data/shortestPathMax");
            if (fs.exists(maxOut)) {
                fs.delete(maxOut, true);
            }

            Configuration config = new Configuration(hadoopConf);
            config.setInt("reduceColumn", 3);

            Job job = Job.getInstance(config, "Shortest Path Max");

            job.setJarByClass(GiraphCompactnessOptimizer.class);
            job.setMapperClass(GlobalReductionMapper.class);
            job.setCombinerClass(ShortestPathGlobalMaxReducer.class);
            job.setReducerClass(ShortestPathGlobalMaxReducer.class);
            job.setOutputKeyClass(NullWritable.class);
            job.setOutputValueClass(Text.class);
            job.setInputFormatClass(TextInputFormat.class);
            job.setOutputFormatClass(TextOutputFormat.class);
            TextInputFormat.addInputPath(job, shortestPathDir);
            TextOutputFormat.setOutputPath(job, maxOut);

            job.waitForCompletion(true);

            java.nio.file.Path localOut = Files.createTempFile("shortestPathMax", "");
            HdfsUtil.copyAndMerge(fs, maxOut, localOut);
            try (BufferedReader br = new BufferedReader(new InputStreamReader(Files.newInputStream(localOut)))) {
                String line = br.readLine();
                return Integer.parseInt(line);
            }
        } catch (IOException | ClassNotFoundException e) {
            throw new RuntimeException(e);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            throw new RuntimeException(e);
        }
    }

    private int getShortestPathLengthSum(Path shortestPathDir) {
        try {
            Path lengthSumOut = new Path("data/shortestPathLengthSum");
            if (fs.exists(lengthSumOut)) {
                fs.delete(lengthSumOut, true);
            }

            Configuration config = new Configuration(hadoopConf);
            config.setInt("reduceColumn", 3);

            Job job = Job.getInstance(config, "Shortest Path Length Sum");

            job.setJarByClass(GiraphCompactnessOptimizer.class);
            job.setMapperClass(GlobalReductionMapper.class);
            job.setCombinerClass(ShortestPathLengthSumReducer.class);
            job.setReducerClass(ShortestPathLengthSumReducer.class);
            job.setOutputKeyClass(NullWritable.class);
            job.setOutputValueClass(Text.class);
            job.setInputFormatClass(TextInputFormat.class);
            job.setOutputFormatClass(TextOutputFormat.class);
            TextInputFormat.addInputPath(job, shortestPathDir);
            TextOutputFormat.setOutputPath(job, lengthSumOut);

            job.waitForCompletion(true);

            java.nio.file.Path localOut = Files.createTempFile("shortestPathLengthSum", "");
            HdfsUtil.copyAndMerge(fs, lengthSumOut, localOut);
            try (BufferedReader br = new BufferedReader(new InputStreamReader(Files.newInputStream(localOut)))) {
                String line = br.readLine();
                return Integer.parseInt(line);
            }
        } catch (IOException | ClassNotFoundException e) {
            throw new RuntimeException(e);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            throw new RuntimeException(e);
        }
    }

    private void join(List<Path> inputPaths, List<Integer> keyIndices, Path outputPath, String separator) {
        try {
            Configuration config = new Configuration(hadoopConf);
            config.set("separator", separator);
            config.setInt("joinSites", inputPaths.size());

            for (int i = 0; i < inputPaths.size(); i++) {
                Path path = inputPaths.get(i);

                config.setInt(path.getName() + "-order", i);
                config.setInt(path.getName() + "-keyIndex", keyIndices.get(i));
            }

            Job job = Job.getInstance(config, "Join");

            job.setJarByClass(GiraphCompactnessOptimizer.class);
            job.setMapperClass(JoinMapper.class);
            job.setReducerClass(JoinReducer.class);
            job.setMapOutputKeyClass(Text.class);
            job.setOutputKeyClass(NullWritable.class);
            job.setOutputValueClass(Text.class);
            job.setInputFormatClass(TextInputFormat.class);
            job.setOutputFormatClass(TextOutputFormat.class);
            for (Path inputPath : inputPaths) {
                TextInputFormat.addInputPath(job, inputPath);
            }
            TextOutputFormat.setOutputPath(job, outputPath);

            job.waitForCompletion(true);
        } catch (IOException | ClassNotFoundException e) {
            throw new RuntimeException(e);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            throw new RuntimeException(e);
        }
    }

    private Graph extendBaseGraph(Set<Vertex> baseVertices, Set<Edge> baseEdges, Graph extension) {
        Set<Vertex> extendedVertices = new HashSet<>(baseVertices);
        extendedVertices.addAll(extension.getVertices());

        Set<Edge> extendedEdges = new HashSet<>(baseEdges);
        extendedEdges.addAll(extension.getEdges());

        return new Graph(extendedVertices, extendedEdges);
    }
}
