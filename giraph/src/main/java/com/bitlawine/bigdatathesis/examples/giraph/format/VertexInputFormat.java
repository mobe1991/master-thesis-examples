package com.bitlawine.bigdatathesis.examples.giraph.format;

import org.apache.giraph.edge.Edge;
import org.apache.giraph.edge.EdgeFactory;
import org.apache.giraph.graph.Vertex;
import org.apache.giraph.io.formats.TextVertexInputFormat;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.InputSplit;
import org.apache.hadoop.mapreduce.RecordReader;
import org.apache.hadoop.mapreduce.TaskAttemptContext;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author Moritz Becker on 16/06/2017
 */
public class VertexInputFormat extends TextVertexInputFormat<LongWritable, ShortestPathMap, NullWritable> {

    @Override
    public TextVertexReader createVertexReader(InputSplit split, TaskAttemptContext context) throws IOException {
        return new CustomTextVertexReader();
    }

    class CustomTextVertexReader extends TextVertexReader {

        private RecordReader<LongWritable, Text> recordReader;

        @Override
        public void initialize(InputSplit inputSplit, TaskAttemptContext context) throws IOException, InterruptedException {
            super.initialize(inputSplit, context);
            this.recordReader = getRecordReader();
        }

        @Override
        public boolean nextVertex() throws IOException, InterruptedException {
            return recordReader.nextKeyValue();
        }

        @Override
        public Vertex<LongWritable, ShortestPathMap, NullWritable> getCurrentVertex() throws IOException, InterruptedException {
            String line = recordReader.getCurrentValue().toString();
            String[] parts = line.split(";");
            Long vertexId = Long.valueOf(parts[0]);
            List<Long> outVertexIds = Arrays.stream(parts[1].substring(1, parts[1].length() - 1).split(","))
                    .filter(s -> !s.isEmpty())
                    .map(Long::valueOf)
                    .collect(Collectors.toList());

            Set<Edge<LongWritable, NullWritable>> edges = new HashSet<>();
            for (Long outVertexId : outVertexIds) {
                edges.add(EdgeFactory.create(new LongWritable(outVertexId), NullWritable.get()));
            }

            Vertex<LongWritable, ShortestPathMap, NullWritable> vertex = getConf().createVertex();
            vertex.initialize(new LongWritable(vertexId), new ShortestPathMap(), edges);
            return vertex;
        }
    }
}
