package com.bitlawine.bigdatathesis.examples.giraph;

import com.bitlawine.bigdatathesis.examples.common.graph.TopologyOptimizationExecutor;

import java.io.IOException;

/**
 * @author Moritz Becker on 16/06/2017
 */
public class GiraphRunner {

    public static void main(String[] args) throws IOException {
        if (args.length != 3) {
            System.out.println("Usage: <topology-name> <num-new-stations> <jar-location>");
            System.exit(1);
        }
        String topologyName = args[0];
        int numNewStations = Integer.parseInt(args[1]);
        String jarLocation = args[2];
        TopologyOptimizationExecutor.run(new GiraphCompactnessOptimizer(jarLocation), topologyName, numNewStations);
    }
}
