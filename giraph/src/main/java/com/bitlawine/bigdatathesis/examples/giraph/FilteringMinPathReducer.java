package com.bitlawine.bigdatathesis.examples.giraph;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;
import java.util.Comparator;
import java.util.stream.StreamSupport;

/**
 * @author Moritz Becker on 26/06/2017
 */
public class FilteringMinPathReducer extends Reducer<LongWritable, Text, NullWritable, Text> {

    private Text valueWrapper = new Text();

    @Override
    protected void reduce(LongWritable key, Iterable<Text> values, Context context) throws IOException, InterruptedException {
        String minValue = StreamSupport.stream(values.spliterator(), false)
                .map(Object::toString)
                .min(Comparator.comparingInt(
                        v -> Integer.parseInt(v.split(";")[3])
                )).get();

        valueWrapper.set(minValue);
        context.write(NullWritable.get(), valueWrapper);
    }
}
