package com.bitlawine.bigdatathesis.examples.giraph;

import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.util.*;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

/**
 * @author Moritz Becker on 24/06/2017
 */
public class JoinReducer extends Reducer<Text, Text, NullWritable, Text> {

    private Text joinedText = new Text();
    private StringBuilder builder = new StringBuilder();

    @Override
    protected void reduce(Text key, Iterable<Text> values, Context context) throws IOException, InterruptedException {
        String separator = context.getConfiguration().get("separator");
        int joinSites = context.getConfiguration().getInt("joinSites", -1);
        Map<Integer, List<String>> joinTuplesPerSite = StreamSupport.stream(values.spliterator(), false)
                .map(value -> value.toString().split(separator))
                .collect(Collectors.toMap(
                        valueParts -> Integer.parseInt(valueParts[0]),
                        valueParts -> new ArrayList<>(Collections.singletonList(Arrays.asList(valueParts).subList(1, valueParts.length).stream().collect(Collectors.joining(separator)))),
                        (a, b) -> { a.addAll(b); return a; }
                ));

        join(joinTuple -> {
            builder.append(key.toString()).append(separator);
            for (String value : joinTuple) {
                builder.append(value).append(separator);
            }
            builder.setLength(builder.length() - 1);
            joinedText.set(builder.toString());
            try {
                context.write(NullWritable.get(), joinedText);
            } catch (IOException e) {
                throw new UncheckedIOException(e);
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
                throw new RuntimeException(e);
            }
            builder.setLength(0);
        }, joinTuplesPerSite, joinSites, 0, new ArrayList<>(joinSites));
    }

    private void join(Consumer<List<String>> joinTupleConsumer, Map<Integer, List<String>> joinTuplesPerSite, int joinSites, int currentJoinSite, List<String> currentJoinTuple) {
        if (currentJoinSite < joinSites) {
            if (joinTuplesPerSite.containsKey(currentJoinSite)) {
                for (String tuple : joinTuplesPerSite.get(currentJoinSite)) {
                    currentJoinTuple.add(tuple);
                    join(joinTupleConsumer, joinTuplesPerSite, joinSites, currentJoinSite + 1, currentJoinTuple);
                    currentJoinTuple.remove(currentJoinTuple.size() - 1);
                }
            }
        } else {
            joinTupleConsumer.accept(currentJoinTuple);
        }
    }
}
