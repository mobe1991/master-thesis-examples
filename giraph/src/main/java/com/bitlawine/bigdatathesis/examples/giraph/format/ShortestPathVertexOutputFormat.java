package com.bitlawine.bigdatathesis.examples.giraph.format;

import org.apache.giraph.graph.Vertex;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.TaskAttemptContext;

import java.io.IOException;
import java.util.Map;

/**
 * @author Moritz Becker on 19/06/2017
 */
public class ShortestPathVertexOutputFormat extends org.apache.giraph.io.formats.TextVertexOutputFormat<LongWritable, ShortestPathMap, NullWritable> {
    @Override
    public TextVertexWriter createVertexWriter(TaskAttemptContext context) throws IOException, InterruptedException {
        return new TextVertexWriter() {
            @Override
            public void writeVertex(Vertex<LongWritable, ShortestPathMap, NullWritable> vertex) throws IOException, InterruptedException {
                for (Map.Entry<Long, Integer> shortestPathEntry : vertex.getValue().entrySet()) {
                    getRecordWriter().write(new Text(String.format("%d;%d;%d", vertex.getId().get(), shortestPathEntry.getKey(), shortestPathEntry.getValue())), null);
                }
//                getRecordWriter().write(new Text(ShortestPathVertexOutputFormat.toString(
//                        vertex.getId().get(),
//                        null,
//                        vertex.getValue()
//                )), null);
            }
        };
    }

//    public static String toString(Long vertexId, Collection<Long> outVertexIds, Map<Long, Integer> shortestPaths) {
//        String outVertexIdStr = outVertexIds == null ? null : outVertexIds.stream()
//                .map(Object::toString)
//                .collect(Collectors.joining(",", "[", "]"));
//        String shortestPathStr = shortestPaths.entrySet().stream()
//                .map(entry -> "(" + entry.getKey().toString() + "," + entry.getValue() + ")")
//                .collect(Collectors.joining(",", "[", "]"));
//        if (outVertexIdStr == null) {
//            return String.format("%d;%s",
//                    vertexId,
//                    shortestPathStr);
//        } else {
//            return String.format("%d;%s;%s",
//                    vertexId,
//                    outVertexIdStr,
//                    shortestPathStr);
//        }
//    }
}
