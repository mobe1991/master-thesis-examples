package com.bitlawine.bigdatathesis.examples.giraph.format;

import org.apache.hadoop.io.Writable;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Moritz Becker on 16/06/2017
 */
public class ShortestPathMap extends HashMap<Long, Integer> implements Writable {

    @Override
    public void write(DataOutput out) throws IOException {
        out.writeInt(size());
        for (Map.Entry<Long, Integer> entry : entrySet()) {
            out.writeLong(entry.getKey());
            out.writeInt(entry.getValue());
        }
    }

    @Override
    public void readFields(DataInput in) throws IOException {
        clear();
        int size = in.readInt();
        for (int i = 0; i < size; i++) {
            long vertexId = in.readLong();
            int shortestPathLength = in.readInt();
            put(vertexId, shortestPathLength);
        }
    }
}
